﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConvertObjectHandler : Handler {

    private PassageEndHandler endHandler;
    private Report report;

    private Text preamble;
    private Button startButton;
    private StoryObject target;
    private List<Button> cardButtons = new List<Button>();

    public override void Awake(){
        base.Awake();
        int children = transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Preamble":
                    preamble = child.GetComponent<Text>();
                    AddListeners(child);
                    break;
                case "Start":
                    startButton = child.GetComponent<Button>();
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }

    public override void Init(bool newCard, Passage passage, Recordable recordable){
        base.Init(newCard, passage);
        if (newCard){
            report = new Report();
            startButton.onClick.AddListener(FinishConvert);
            cardButtons = storyManager.InitialiseButtons(passage.applicableTypes, "RETIRED_ONLY");
            StoryManager.selectDelegate += ChooseConvert;
            if (cardButtons.Count == 1){
                cardButtons[0].onClick.Invoke();
            } else if (cardButtons.Count == 0) {
                EndPremature();
            }
        } else {
            this.passage = passage;
            report = recordable as Report;
            startButton.gameObject.SetActive(false);
            endHandler = storyManager.AddPassageEnd();
            endHandler.SetText(report.result);
            endHandler.DisableButton(true);
            target = report.chosenStoryObject;
        }
        CalculatePreamble();
    }

    private void CalculatePreamble(){
        string source = "";
        if (target != null) {
            source = target.GetFullName();
        } else {
            if (report.applicableTypestring == null){
                source = passage.applicableTypeString("/");
                report.applicableTypestring = source;
                string article = "a";
                foreach(char vowel in "aeiou"){
                    if (source.ToLower()[0] == vowel){
                        article = "an";
                        break;
                    }
                }
                source = article + " " + source;
            }
        }
        preamble.text = "Convert " + source + " to " + passage.objectType;
    }

    void FinishConvert() {
        if (target == null) return;
        Convert(1);
    }

    void Convert(int power){
        startButton.gameObject.SetActive(false);
        string result;
        if (power > 0) {
            target.AdjustPower(power);
            result = "Changed " + target.GetName() + " from " + target.label + " to " + passage.objectType + " and restored to " + target.GetPower().ToString() + " Power.";
            storyManager.storyHolder.ConvertStoryObject(target, passage.objectType);
        }
        else {
            result = "Found nothing to convert.";
        }

        endHandler = storyManager.AddPassageEnd();
        endHandler.SetText(result);
        endHandler.AddListener(ReturnStory);
    }

    void EndPremature(){
        Convert(0);
    }

    void ChooseConvert(){
        StoryObject choice = storyManager.selectedObject;

        if (!passage.isApplicableType(choice.label)){
            storyManager.CallBlock("wrongTypeConvert");
            return;
        }
        if (!choice.retired){
            storyManager.CallBlock("unretiredConvert");
            return;
        }
        target = choice;
        CalculatePreamble();
    }

    public override void ReturnStory(){
        storyManager.ClearCardButtons(cardButtons);
        report.prefabName = "convert_object";
        report.chosenStoryObject = target;
        endHandler.DisableButton(true);
        storyManager.storyHolder.story.Add(report);
        storyManager.FinishScene();
    }
}
