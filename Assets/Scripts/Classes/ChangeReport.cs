﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ChangeReport : Report {

    public Field chosenField;
    public Prompt oldPrompt;
    public Prompt newPrompt;

    public string ToString(bool verbose){
        if (!verbose) return ToString();
        string s = header + "\n" + result;
        s += "\n\nField:\n" + chosenField.ToString();
        s += "\n\nOldPrompt:\n" + oldPrompt.ToString();
        s += "\n\nNewPrompt:\n" + newPrompt.ToString();
        return s;
    }

    public override string ToString(){
        string text = header;
        return text + result;
    }

    public override string ToHtml(){
        string text = MakeHeader(header, 2);
        text += "\n" + MakeHeader(result, 3);
        return text;
    }
}
