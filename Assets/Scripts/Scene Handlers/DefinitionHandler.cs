﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefinitionHandler : Handler {

    public GameObject storyObjectCardPrefab;
    public GameObject card;
    public StoryObject myObject;
    private StoryObjectHandler objectHandler;

    public override void Awake()
    {
        base.Awake();
        myObject = storyManager.gameManager.templates.templateObjects[passage.objectType].Copy();
        myObject.Roll(passage.modifier);
        card = storyManager.AddUI(storyObjectCardPrefab);
        //card = Instantiate(storyObjectCardPrefab);
        //card.transform.SetParent(storyManager.canvas, false);
        objectHandler = card.GetComponent<StoryObjectHandler>();
        objectHandler.Init(true, passage);
    }

    public override void ReturnStory(){
        //objectHandler.Dock(storyManager.objectCards.Count);
        StoryObject snapshot = objectHandler.me.Copy();
        storyManager.storyHolder.Store(snapshot);
        storyManager.storyHolder.story.Add(snapshot);
        storyManager.FinishScene();
    }
}
