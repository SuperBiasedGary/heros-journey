﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OverlayHandler : MonoBehaviour {
	void Awake () {
        return;
        string updatedname = this.gameObject.name + " - saved";
        GameObject gameObject = GameObject.Find(updatedname);
        if (gameObject != null){
            Debug.Log("Destroying");
            Destroy(this.gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
        DontDestroyOnLoad(FindObjectOfType<EventSystem>().gameObject);
        this.gameObject.name = this.gameObject.name + " - saved";
    }
}
