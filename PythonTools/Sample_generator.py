import json
import os
import re

from string import ascii_letters as letters

from random import sample, random


BASE = r"C:\Users\gary.chadwick\Downloads"
BASE = r"C:\Users\gchadwick\Documents\05_Other\heros-journey\Assets\StreamingAssets"
#BASE = r"C:\Users\Gary\Documents\Unity Projects\heros-journey\Assets\StreamingAssets"

VOWELS = "aeiou"
VOWELSY = "aeiouy"


STORY = """\
Our story starts in the Setting:
[{}]

With our Hero:
[{}]

Who is tasked with a Quest:
[{}]

They are helped by their Ally:
[{}]

But they must overcome the Obstacles:
[{}]
and
[{}]

While the Antagonist,
[{}]
is working directly against the Quest."""


STORY = """\
The Climber:
[{}]

must climb the Mountain:
[{}]

passing through Obstacles:
[{}]
,
[{}]
and
[{}]"""


PATH = os.path.join(BASE, "mountain_words.json")
NAME_PATH = os.path.join(BASE, "names.json")
#PATH = r"words.json"
#NAME_PATH = "names.json"
BROAD_PATTERN = re.compile(r"(\[.*?\])")
PATTERN = re.compile(r"^\[(\^\??)?([-*+])?(.*?)(\+\??)?\]$")

class CHANCE:
    NONE = 0
    COINFLIP = 0.5
    DEFINITE = 1


TENSES = {
            "-": "Past",
            "+": "Present",
            "*": "Participle"
            }

class Phrase(object):
    def __init__(self, parent, silent=True, basic=False):
        self.parent = parent
        if basic:
            self.source = "[Adjective] [Noun]"
        else:
            self.source = self.get_phrase()
        self.chunks = [self.source]
        self.parse_strings(self.source)
        self.convert_words()
        self.sort_chunks()
        if not silent:
            print self.source
            print str(self)
            print "\n"

    def get_phrase(self):
        phrase = ""
        for phrase_list in DATA["Phrases"][self.parent]:
            pick = sample(phrase_list, 1)
            phrase += pick[0] + " \n"
        return phrase.strip()

    def parse_strings(self, source):
        results = BROAD_PATTERN.findall(source)
        self.strings = []
        while source:
            if not results:
                block = source
            elif source.startswith(results[0]):
                block = results.pop(0)
            else:
                block = source[:source.find(results[0])]
            self.strings.append(block)
            source = source.replace(block, "", 1)

    def convert_words(self):
        self.words = []
        for string in self.strings:
            phrase = string.strip("[]")
            word = Word(string, self.parent)
            if word.valid:
                self.words.append(word)
            elif phrase in DATA["Phrases"]:
                phrase = Phrase(self.parent, basic=True)
                self.words.append(phrase)
            else:
                self.words.append(string)

    def sort_chunks(self):
        found_noun = False
        chunk = []
        self.chunks = [chunk]
        #for i in range(len(self.chunks) - 1, 0, -1):
        for word in reversed(self.words):
            if not isinstance(word, Word):
                chunk.insert(0, word)
                continue
            if not found_noun:
                found_noun = word.word_type == "Noun"
                if found_noun:
                    chunk = [word]
                    self.chunks.insert(0, chunk)
                    continue
            if found_noun and word.word_type == "Adjective":
                chunk.insert(0, word)
            else:
                found_noun = False
                chunk = [word]
                self.chunks.insert(0, chunk)

    def __str__(self):
        bits = []
        for chunk in self.chunks:
            capitals = any(isinstance(word, Word) and word.capitalised
                           for word in chunk)
            for word in reversed(chunk):
                if hasattr(word, "article") and word.article:
                    article = word.article
                    if article == "a" and startswith_vowel(chunk[0]):
                        article = "an"
                    #chunk.insert(0, article + " ")
                    break
            if capitals:
                bits.append("".join(str(word).capitalize() for word in chunk))
            else:
                bits.append("".join(str(word) for word in chunk))
        result = "".join(bit for bit in bits if bit.strip())
        while "  " in result:
            result = result.replace("  ", " ")
        for vowel in VOWELS:
            target = " a " + vowel
            new = " an " + vowel
            result = result.replace(target, new)
        return result


class Word(object):
    def __init__(self, string, parent=""):
        self.parent = parent
        self.string = string
        match = PATTERN.match(string)
        self.valid = bool(match)
        if not self.valid:
            return
        self.parse(match)
        self.article = ""
        self.pick()
        if self.valid:
            self.styled = self.get_styled()

    def parse(self, match):
        title, tense, base, plural = match.groups()
        self.tense = tense
        if title == "^?":
            self.title = CHANCE.COINFLIP
        elif title == "^":
            self.title = CHANCE.DEFINITE
        else:
            self.title = CHANCE.NONE
        if plural == "+?":
            self.plural = CHANCE.COINFLIP
        elif plural == "+":
            self.plural = CHANCE.DEFINITE
        else:
            self.plural = CHANCE.NONE
        if ":" in base:
            self.word_type, self.tag = base.split(":", 1)
        else:
            self.word_type = base
            self.tag = self.parent

    def pick(self):
        try:
            wordlist = DATA[self.word_type]
        except KeyError:
            self.valid = False
            return

        sample_words = [word for word, values in wordlist.items()
                        if self.tag in values.get("tags", [])]

        # Ensure only compatible words are used for definites.
        if self.tense:
            sample_words = [word for word in sample_words
                            if wordlist[word].get(TENSES[self.tense])]
        if self.plural == CHANCE.DEFINITE:            
            sample_words = [word for word in sample_words
                            if wordlist[word].get("plural")]
        if not sample_words:
            print "No samples available for {} tagged {}".format(self.string, self.tag)
        self.word = sample(sample_words, 1)[0]
        self.word_data = wordlist[self.word]
        if self.word_data.get("articles"):
            self.article = sample(self.word_data["articles"], 1)[0]
    
    def get_styled(self):
        self.capitalised = False
        if self.tense:
            self.word = self.word_data.get(TENSES[self.tense])
        
        if random() < self.title:
            self.capitalised = True
            return self.word.capitalize()
        elif self.word_data.get("plural") and random() < self.plural:
            if self.article != "the":
                self.article = ""
            return self.word_data["plural"]
        return self.word

    def __str__(self):
        return self.styled if self.valid else self.string


class Name(Word):
    def __init__(self, procgen_chance=0):
        self.word_type = "Name"
        if random() < procgen_chance:
            self.string = self.generate_name()
        else:
            self.string = sample(NAME_DATA["Pregen"], 1)[0]
        self.capitalised = self.string
    
    def __str__(self):
        return self.string.capitalize()
    
    def generate_name(self):
        template = sample(NAME_DATA["Templates"], 1)[0]
        pieces = [sample(NAME_DATA["Segments"][c],1)[0] if c in NAME_DATA["Segments"] else c for c in template] 
        result = pieces[0] 
        for piece in pieces[1:]:
            # If there are two adjoining vowels, add a bridging letter
            if result[-1] in VOWELS and piece[0] in VOWELS:
                result += sample(NAME_DATA["Segments"]["t"], 1)[0]
            # If there are going to be three consonants in a row, add a vowel
            # May not be necessary if chunks are constructed intelligently?
            # (is that already the case?)
            elif (len(result) > 2 and len(piece) > 2 and
                  result[-2] not in VOWELSY and piece[1] not in VOWELSY
                  and result[-1] not in VOWELSY and piece[0] not in VOWELSY):
                print "adding vowel between",result,"and",piece
                result += sample(VOWELS, 1)[0]
            result += piece.capitalize()
        return result


def startswith_vowel(text):
    for c in str(text):
        if c in letters:
            return c in VOWELS


def generate(DATA, typename="", silent=True):
    r = not bool(typename)
    if not typename:
        [typename] = sample(DATA["Phrases"].keys(), 1)
    phrase = Phrase(typename, silent)
    if r: return "{}: {}".format(typename, phrase)
    return phrase


def generate_story(DATA):

    phrases = [generate(DATA, key) for key in ("Setting", "Hero", "Quest", "Ally", "Obstacle", "Obstacle", "Antagonist")]
    return STORY.format(*phrases)


def generate_story(DATA):

    phrases = [generate(DATA, key) for key in ("Climber", "Mountain", "Obstacle", "Obstacle", "Obstacle")]
    return STORY.format(*phrases)


def main():
    #results = ["{}: {}".format(generate(DATA, "Title")[0], generate(DATA, "Hero")[0]) for _ in range(100)]
    #results = [str(generate(DATA, "Hero")) for _ in range(100)]
    #results = [generate(DATA, "Quest", silent=False) for _ in range(100)]
    #for _ in range(100): print str(generate(DATA, silent=True))
    for _ in range(100): print "\n" * 3 + generate_story(DATA) + "\n" * 5
    return
    #results.sort()
    for result in results:
        print result
    raw_input()


with open(PATH) as f:
    DATA = json.load(f)
#with open(NAME_PATH) as f:
#    NAME_DATA = json.load(f)

main()
