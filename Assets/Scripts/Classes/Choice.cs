﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Choice : Recordable {

    public float weight = 0f;
    public string label;
    public string tooltip;
    public string cues;
    public string flag;

    public string function;
    public string requirement;
    public string selector;
    public string[] applicableTypes;

    public string modifierName;
    public string modifierOperation;

    public bool chosenPrompt;
    public bool active = false;

    public override string ToString(){
        if (!active) return label + " [INACTIVE]";
        if (chosenPrompt) return label + " [CHOSEN]"; ;
        return label;
    }

    public string ToString(bool verbose){
        string baseString = ToString();
        if (!verbose) return baseString;
        return (baseString + "\nfunction: " + function
                           + "\nrequirement: " + requirement
                           + "\nselector: " + selector
                           + "\napplicableTypes: " + applicableTypes.ToString()
                           + "\nmodifierName: " + modifierName
                           + "\nmodifierOperation: " + modifierOperation);
    }

    public override string ToHtml(){
        if (!active) return MakeStrikethrough(MakeHeader(label + " - Inactive", 6));
        if (chosenPrompt) {
            string result = label + " - " + MakeBold("[CHOSEN]");
            return MakeHeader(result, 6);
        }
        return MakeHeader(label, 6);
    }
}
