﻿using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class DataManager {

    private const string SETTINGS_SAVE = "settings.data";
    private const string TEMP_SAVE = "story.data";

    public static void SaveSettings(GameSettings settings){
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + SETTINGS_SAVE);
        binaryFormatter.Serialize(file, settings);
        file.Close();
    }

    public static GameSettings LoadSettings(GameSettings oldSettings){
        GameSettings gameSettings = null;
        if (File.Exists(Application.persistentDataPath + "/" + SETTINGS_SAVE)){
            Debug.Log("Loading from " + Application.persistentDataPath + "/" + SETTINGS_SAVE);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/" + SETTINGS_SAVE, FileMode.Open);
            gameSettings = (GameSettings) binaryFormatter.Deserialize(file);
            file.Close();
        }
        if (gameSettings == null){
            gameSettings = new GameSettings();
            gameSettings.initExportFolder();
        }
        return gameSettings;
    }


    public static void SaveStory(StoryHolder storyHolder){
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/" + TEMP_SAVE;
        FileStream file = File.Create(path);
        binaryFormatter.Serialize(file, storyHolder);
        file.Close();
    }

    public static StoryHolder LoadStory(){
        StoryHolder storyHolder = null;
        if (File.Exists(Application.persistentDataPath + "/" + TEMP_SAVE)){
            Debug.Log("Loading from " + Application.persistentDataPath + "/" + TEMP_SAVE);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/" + TEMP_SAVE, FileMode.Open);
            try {
                storyHolder = (StoryHolder)binaryFormatter.Deserialize(file);
            } catch (System.Exception exception){
                Debug.LogWarning(exception.Message);
                Debug.LogWarning(exception.StackTrace);
            } finally {
                file.Close();
            }
        }
        return storyHolder;
    }
}
