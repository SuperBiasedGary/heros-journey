﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NarrativeHandler : Handler
{

    private Text typeDisplay;
    private Text promptDisplay;

    private InputField textEntry;
    private Text textDisplay;
    private Button submit;

    public override void Awake(){
        base.Awake();
        int children = transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Sample":
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
                case "Type":
                    typeDisplay = child.GetComponent<Text>();
                    AddListeners(child);
                    break;
                case "Prompt":
                    promptDisplay = child.GetComponent<Text>();
                    AddListeners(child);
                    break;
                case "Passage Field":
                    textEntry = child.GetComponent<InputField>();
                    AddListeners(child);
                    break;
                case "Passage Display":
                    textDisplay = child.GetComponent<Text>();
                    break;
                case "Submit":
                    submit = child.GetComponent<Button>();
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }

    public override void Init(bool newCard, Passage passage){
        base.Init(newCard, passage);
        passage.prefabName = "narrative";
        typeDisplay.text = passage.label;
        typeDisplay.GetComponent<TooltipHolder>().tooltip = passage.tooltip;
        typeDisplay.GetComponent<TooltipHolder>().cues = passage.cues;

        if (passage.chosenPrompt == null) {
            promptDisplay.text = "";
        } else {
            promptDisplay.text = passage.chosenPrompt.label;
            promptDisplay.GetComponent<TooltipHolder>().tooltip = passage.chosenPrompt.tooltip;
            promptDisplay.GetComponent<TooltipHolder>().cues = passage.chosenPrompt.cues;
        }

        if (newCard) {
            submit.onClick.AddListener(SetFinished);
        } else {
            submit.gameObject.SetActive(false);
            textEntry.gameObject.SetActive(false);
            textDisplay.text = passage.text;
        }
    }



    public override void ReturnStory(){
        if (textEntry.text == ""){
            storyManager.CallBlock("missingParagraph");
            return;
        }
        passage.text = textEntry.text;
        textDisplay.text = textEntry.text;
        textEntry.gameObject.SetActive(false);
        submit.gameObject.SetActive(false);
        storyManager.storyHolder.story.Add(passage);
        storyManager.FinishScene();
    }
}
