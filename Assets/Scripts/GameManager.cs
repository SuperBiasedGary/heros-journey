﻿using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public GameSettings gameSettings;
    public Templates templates;
    public Text tooltip;
    public Text cues;
    public JSONParser jsonParser;
    public GameObject storyManagerPrefab;
    public Flowchart flowchart;
    public StoryManager storyManager;

    void Awake() {
        DontDestroyOnLoad(gameObject);
        SceneManager.LoadScene("MainMenu");
        gameSettings = DataManager.LoadSettings(gameSettings);
        flowchart = FindObjectOfType<Flowchart>();
        EventSystem eventSystem = FindObjectOfType<EventSystem>();
        DontDestroyOnLoad(eventSystem.gameObject);
        jsonParser = GetComponent<JSONParser>();
    }

    private void Update(){
        if ((Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl)) && Input.GetButtonDown("Exit")) { 
            SceneManager.LoadScene("MainMenu");
        }
    }

    public void LoadGame(){
        StoryHolder storyHolder = DataManager.LoadStory();
        if (storyHolder == null){
            Debug.LogWarning("No save data found.");
            return;
        }
        //StoryManager storyManager = Instantiate(storyManagerPrefab).GetComponent<StoryManager>();
        Debug.Log("Finding StoryManager");
        storyManager = FindObjectOfType<StoryManager>();
        storyManager.Init();
        storyManager.storyHolder = storyHolder;
        storyManager.Load();
    }

    public void ActivateStory(){
        //GameObject gameObject = Instantiate(storyManagerPrefab);
        //storyManager = gameObject.GetComponent<StoryManager>();
        Debug.Log("Finding StoryManager");
        storyManager = FindObjectOfType<StoryManager>();

        SceneManager.LoadScene("StartStory");
        storyManager.CallBlock("Tutorial Check");
    }

    public void StartStory(){
        if (storyManager == null){
            Debug.Log("No storymanager found");
        } else {
            Debug.Log("Storymanager found");
        }
        storyManager.Init();
        storyManager.StartStory();
        Debug.Log("StartedStory");
    }

    public void PromptGenerator(){
        SceneManager.LoadScene("PromptGenerator");
    }

    public bool NeedTutorial(){
        return gameSettings.tutorials;
    }

    public void DisableTutorial(){
        gameSettings.tutorials = false;
    }

}

