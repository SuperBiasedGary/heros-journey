﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryObjectHolder : MonoBehaviour {
    public StoryObject storyObject;
    public StoryManager storyManager;
    public bool selectorActive = false;

    private void Awake(){
        storyManager = FindObjectOfType<StoryManager>();
    }

    public void SelectMe(){
        
    }
}
