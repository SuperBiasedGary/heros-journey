﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderHandler : MonoBehaviour {

    public Text title;
    public Text counter;
    public Slider slider;
    public int value;

    private void Awake(){
        int children = transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Title":
                    title = child.GetComponent<Text>();
                    break;
                case "Counter":
                    counter = child.GetComponent<Text>();
                    break;
                case "Slider":
                    slider = child.GetComponent<Slider>();
                    slider.onValueChanged.AddListener(UpdateCounter);
                    break;
            }
        }
    }

    public void UpdateCounter(float value){
        this.value = (int) value;
        counter.text = value.ToString();
        slider.value = value;
    }
}
