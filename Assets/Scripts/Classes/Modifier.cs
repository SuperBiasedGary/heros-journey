﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Modifier : Recordable {

    public string tooltip;
    public int modifier;

    public override string ToHtml(){
        string result;
        if (modifier > 0) result = "+" + modifier.ToString() + " coins: " + tooltip;
        else result = modifier.ToString() + " coins: " + tooltip;
        return MakeParagraph(result);
    }

    public override string ToString(){
        string result;
        if (modifier > 0) result = "+" + modifier.ToString();
        else result = modifier.ToString();
        return result + " coins: " + tooltip;
    }
}
