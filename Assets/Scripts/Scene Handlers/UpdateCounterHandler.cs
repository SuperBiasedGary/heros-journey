﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UpdateCounterHandler : Handler {

    public override void Awake(){
        base.Awake();
    }

    public override void Init(bool newCard, Passage passage){
        base.Init(newCard, passage);
        if (!newCard) return;

        string operation = passage.objectTypeFunction;
        string counterName = passage.objectType;
        Operate(operation, counterName);
        ReturnStory();
    }

    public void Operate(string operation, string counterName) {
        
        switch (operation){
            case "+":
                storyManager.storyHolder.AddToCounter(counterName, 1);
                break;
            case "-":
                storyManager.storyHolder.AddToCounter(counterName, -1);
                break;
            case "INIT":
                storyManager.storyHolder.InitCounter(counterName);
                break;
            case "GET":
                string flag = storyManager.storyHolder.GetCounter(counterName).ToString();
                Debug.Log("Got counter " + counterName + " value: " + flag);
                storyManager.storyHolder.blockFlag = flag;
                break;
            default:
                Debug.Log("No operation found for " + operation);
                break;
        }
    }

    public override void ReturnStory(){
        storyManager.FinishScene();
    }
}
