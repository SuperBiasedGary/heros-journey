﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SaveHandler : Handler
{
    private InputField textEntry;
    private Text displayText;
    private Button submit;

    private string title;
    private string path;

    public override void Awake(){
        base.Awake();
        GameObject child;
        for (int i = 0; i < transform.childCount; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Title Field":
                    textEntry = child.GetComponent<InputField>();
                    AddListeners(child);
                    break;
                case "Display":
                    displayText = child.GetComponent<Text>();
                    break;
                case "Submit":
                    submit = child.GetComponent<Button>();
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }

    public override void Init(bool newCard, Passage myPassage){
        base.Init(newCard, myPassage);
        submit.onClick.AddListener(SaveStory);
    }

    string BuildStory(){
        StoryHolder storyHolder = storyManager.storyHolder;
        string storyText = "<title>" + title + "</title>\n<b><u><h1>" + title + "</h1></b></u>\n\n";
        foreach (Recordable record in storyHolder.story){
            if (record == null) continue;
            storyText += "\n\n" + record.ToHtml();
        }
        return storyText;
    }
    void WriteStory(string text){
        string folder = storyManager.gameManager.gameSettings.exportFolder;
        if (folder == "" || folder == null){
            Debug.LogWarning("Couldn't find export folder, reverting to default.");
            storyManager.gameManager.gameSettings.initExportFolder();
            folder = storyManager.gameManager.gameSettings.exportFolder;
        }
        if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);
        Debug.Log("Trying to calculate path.");
        path = Path.Combine(folder, title + ".html");
        if (path == null){
            Debug.Log("Found null path.");
            Debug.Log("Was using folder: " + folder);
            path = Path.Combine(folder, "fallback.html");
        }
        Debug.Log("Found path: " + path);
        int i = 0;
        while (File.Exists(path)){
            i++;
            path = Path.Combine(folder, title + "_" + i.ToString() + ".html");
        }
        Debug.Log("Writing file to " + path);
        StreamWriter writer = new StreamWriter(path, true);
        writer.Write(text);
        writer.Close();
    }

    public void SaveStory(){
        if (textEntry.text == ""){
            storyManager.CallBlock("missingTitle");
            return;
        }
        title = textEntry.text;
        // To add them to the story
        storyManager.StringifyFlags();
        string fullStory = BuildStory();
        WriteStory(fullStory);
        submit.onClick.RemoveAllListeners();
        submit.onClick.AddListener(ReturnStory);
        displayText.text = "Wrote story to " + path + "\nClick submit again to return to the title screen.";
    }

    public override void ReturnStory(){
        storyManager.FinishScene();
        Destroy(storyManager.canvas.gameObject);
        SceneManager.LoadScene("MainMenu");
        Destroy(storyManager.gameObject);
    }
}