﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ToggleSpriteSwap : MonoBehaviour
{
    public Toggle targetToggle;
    public Sprite enabledSprite;
    public Sprite disabledSprite;
    public Image checkboxImage;

    void Awake(){
        targetToggle = GetComponent<Toggle>();
        checkboxImage = GetComponentInChildren<Image>();
    }

    void Start(){
        targetToggle.toggleTransition = Toggle.ToggleTransition.None;
        targetToggle.onValueChanged.AddListener(OnTargetToggleValueChanged);
    }

    void OnTargetToggleValueChanged(bool isChecked){
        Image targetImage = targetToggle.targetGraphic as Image;
        if (targetImage == null) return;
        if (isChecked) checkboxImage.sprite = enabledSprite;
        else checkboxImage.sprite = disabledSprite;
    }
}