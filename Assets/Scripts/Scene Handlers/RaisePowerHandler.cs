﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaisePowerHandler : Handler {

    public GameObject resultPrefab;
    private PassageEndHandler endHandler;

    private const float flipTime = 1.3f;
    private Report report;
    private Text preamble;
    private Button startButton;
    private StoryObject trainee;
    private const int FLIP_COUNT = 3;
    private int[] rolls = new int[3];
    private bool[] rolling = new bool[3] { true, true, true };
    private List<Button> cardButtons;

    public override void Awake(){
        base.Awake();
        int children = transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Preamble":
                    preamble = child.GetComponent<Text>();
                    AddListeners(child);
                    break;
                case "Start":
                    startButton = child.GetComponent<Button>();
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }

    public override void Init(bool newCard, Passage passage, Recordable recordable){
        base.Init(newCard, passage);
        if (newCard) {
            report = new Report();
            startButton.onClick.AddListener(StartPowerUp);
            
            cardButtons = storyManager.InitialiseButtons(passage.applicableTypes, "RETIRE_CHECK");
            if (cardButtons.Count == 1) cardButtons[0].onClick.Invoke();
            StoryManager.selectDelegate += ChooseTrainee;
        } else {
            report = recordable as Report;
            trainee = report.chosenStoryObject;
            startButton.gameObject.SetActive(false);

            int flipCount = report.flips.Count;
            string[] flipset = new string[flipCount];
            string[] keys = new string[flipCount];
            int i = 0;

            foreach (string key in report.flips.Keys){
                keys[i] = key;
                i++;
            }
            
            int flipLength = report.flips[keys[0]].Length;
            int j;
            for (i = 0; i < flipLength; i++){
                for (j = 0; j < flipCount; j++) flipset[j] = "";
                for (j = 0; j < flipCount; j++){
                    flipset[j] = report.flips[keys[j]][i];
                }
                storyManager.AddTripleTextScrap(flipset);
            }

            endHandler = storyManager.AddPassageEnd();
            endHandler.SetText(report.result);
            endHandler.DisableButton(true);
        }
        CalculatePreamble();
    }

    private void CalculatePreamble(){
        if (trainee != null) {
            preamble.text = "\nAttempting to train " + trainee.GetFullName();
        } else {
            string typestring = passage.applicableTypeString("/");
            preamble.text = "Attempt to train a " + typestring;
        }
    }

    IEnumerator Flip(int coinCount){
        report.flips["Attempt 1"] = new string[coinCount];
        report.flips["Attempt 2"] = new string[coinCount];
        report.flips["Attempt 3"] = new string[coinCount];
        string flip;
        string[] flipSet = new string[FLIP_COUNT];
        bool stillRolling;
        for (int i = 0; i < coinCount; i++){
            // Check if all of them have already failed.
            stillRolling = false;
            foreach(bool isRolling in rolling){
                if (isRolling){
                    stillRolling = true;
                    break;
                }
            }
            if (!stillRolling) break;
            for (int x = 0; x < flipSet.Length; x++) flipSet[x] = "";
            yield return new WaitForSeconds(flipTime);
            for (int x = 0; x < flipSet.Length; x++){
                if (!rolling[x]) continue;

                if (UnityEngine.Random.value > 0.5){
                    rolls[x]++;
                    flip = "HEADS";
                } else {
                    flip = "TAILS";
                    rolling[x] = false;
                }
                flipSet[x] = flip;
                report.flips["Attempt " + (x + 1).ToString()][i] = flip;
            }
            storyManager.AddTripleTextScrap(flipSet);
        }
        yield return new WaitForSeconds(flipTime);
        for (int x = 0; x < flipSet.Length; x++) flipSet[x] = "";
        bool anySuccess = false;
        for (int x = 0; x < flipSet.Length; x++){
            if (rolling[x]){
                flipSet[x] = "[SUCCESS]";
                anySuccess = true;
            }
        }
        if (anySuccess) storyManager.AddTripleTextScrap(flipSet);
        FinishPowerUp(coinCount);
    }

    void StartPowerUp() {
        if (trainee == null){
            storyManager.CallBlock("wrongTypeTraining");
            return;
        }

        int coinCount = trainee.GetPower() + 1;
        preamble.text += "\nFlipping " + coinCount.ToString() + " coins...";
        StartCoroutine(Flip(coinCount));
        startButton.gameObject.SetActive(false);
    }

    void FinishPowerUp(int coinCount){
        bool perfect = true;
        bool success = false;
        foreach (int roll in rolls){
            if (roll == coinCount) success = true;
            else perfect = false;
        }

        string result;
        if (perfect) {
            result = "Incredible success! Increased Power of " + trainee.GetName() + " by 2";
            trainee.AdjustPower(2);
            storyManager.storyHolder.blockFlag = "CRITICAL";
        } else if (success) {
            result = "Success! Increased Power of " + trainee.GetName() + " by 1";
            trainee.AdjustPower(1);
            storyManager.storyHolder.blockFlag = "SUCCESS";
        }
        else {
            result = "Failure... Didn't raise " + trainee.GetName() + "'s Power.";
            storyManager.storyHolder.blockFlag = "FAILURE";
        }
        report.result = result;

        endHandler = storyManager.AddPassageEnd();
        endHandler.SetText(result);
        endHandler.AddListener(ReturnStory);
    }

    void ChooseTrainee(){
        if (storyManager.selectedObject == null) return;
        StoryObject choice = storyManager.selectedObject;

        if (!passage.isApplicableType(choice.label)){
            storyManager.CallBlock("wrongTypeTraining");
            return;
        }

        if (choice.retired){
            storyManager.CallBlock("retiredTraining");
            return;
        }
        trainee = choice;
        report.chosenStoryObject = trainee;
        CalculatePreamble();
    }

    public override void ReturnStory(){
        report.prefabName = "raise_power";
        endHandler.DisableButton(true);
        storyManager.storyHolder.story.Add(report);
        storyManager.FinishScene();
    }
}
