﻿public class ClearRecentHandler : Handler {

    public override void Awake(){
        base.Awake();
    }

    public override void Init(bool newCard, Passage passage){
        base.Init(newCard, passage);
        if (!newCard) return;

        storyManager.storyHolder.ClearRecent(passage.applicableTypes);
        ReturnStory();
    }

    public override void ReturnStory(){
        storyManager.FinishScene();
    }
}
