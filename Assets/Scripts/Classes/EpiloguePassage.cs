﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EpiloguePassage : Passage {

    public StoryObject storyObject;

    public override string ToString(){
        string result = label;
        result += "\n" + text;
        return result;
    }

    public override string ToHtml(){
        string result = MakeHeader(label, 3);
        return result + "\n" + MakeParagraph(text);
    }
}
