﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModifierHandler : Handler {

    Modifier modifier;
    Text display;
    public float PAUSE_TIME = 1.25f;

    public override void Awake(){
        base.Awake();
        display = transform.GetComponentInChildren<Text>();
    }

    public override void Init(bool newCard, Passage passage, Recordable recordable){
        base.Init(newCard, passage);
        if (newCard){
            modifier = new Modifier();
            modifier.name = passage.label;
            modifier.tooltip = passage.tooltip;
            modifier.modifier = passage.modifier;
            storyManager.storyHolder.modifiers.Add(modifier);
            display.text = modifier.ToString();
            StartCoroutine(PauseThenReturn());
        }
        else {
            modifier = recordable as Modifier;
            display.text = modifier.ToString();
        }
    }

    IEnumerator PauseThenReturn(){
        yield return new WaitForSeconds(PAUSE_TIME);
        finished = true;
    }

    public override void ReturnStory(){
        modifier.prefabName = "quest_modifier";
        storyManager.storyHolder.story.Add(modifier);
        storyManager.FinishScene();
        finished = false;
    }
}
