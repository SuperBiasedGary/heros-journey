import re

from collections import defaultdict
from pprint import pprint
from sys import argv
from copy import deepcopy

try:
    from simplejson import load
except ImportError:
    from json import load

PATTERN = re.compile(r'\s*"(.*?)": {\s*')
DUPLICATE_KEY_EXCEPTIONS = ("goto",)
BLOCK_TARGET_EXCEPTIONS = ["END"]

def check_file(path):
    global blocks
    global passages
    global used_passages
    
    data = read_file(path)
    if not data: return

    check_duplicate_keys(data.keys(), path)
    passages = data["Prompts"].keys()
    used_passages = []
    choice_flags = get_valid_choice_flags(data)
    blocks = data["Blocks"]
    block_keys = blocks.keys() + BLOCK_TARGET_EXCEPTIONS
    for name, block in blocks.items():
        test_block(name, block, passages, block_keys, choice_flags)
    test_endings(blocks)

    unused = sorted(passage for passage in passages
                    if passage not in used_passages)
    for passage in unused:
        print "Passage", passage, "is never used."


    print path, "check complete"

def get_valid_choice_flags(data):

    results = {}
    for name, passage in data["Prompts"].items():
        if not passage["type"] == "choose":
            continue
        results[name] = [choice["flag"] for choice in passage["choices"]]
    return results
                

def test_block(name, block, passages, block_keys, choice_flags):
    global used_passages
    flags = []
    for passage in block["passages"]:
        if passage not in passages:
            print "Passage", passage, "from", name, "not found"
        else:
            used_passages.append(passage)
        if passage in choice_flags:
            flags += choice_flags[passage]
    goto = block["goto"]
    if isinstance(goto, unicode) or isinstance(goto, str):
        if goto not in block_keys:
            print "Block", goto, "from", name, "not found"
    elif isinstance(goto, dict):
        for flag, blockname in goto.items():
            if blockname not in block_keys:
                print "Block", blockname, "from", name, "not found"
            if flags and flag not in flags:
                print "Flag", flag, "from", name, "not found"
    else:
        print "Unsupported goto type in ", name, type(goto)



def check_duplicate_keys(groups, path):

    current = None
    keys = defaultdict(set)
    with open(path) as f:
        for line in f:
            for group in groups:
                if group in line:
                    current = group
                    break
            match = PATTERN.match(line)
            if not match:
                continue
            key = match.group(1)
            if key in DUPLICATE_KEY_EXCEPTIONS:
                continue
            if key in keys[current]:
                print "Duplicate key", key, "in", current
            else:
                keys[current].add(key)
    return keys



def read_file(path):
    try:
        with open(path) as f:
            return load(f)
    except ValueError as e:
        print "Couldn't read JSON"
        print e

                
def test_endings(blocks):

    visited = set()
    try:
        find_start("END", deepcopy(blocks), visited)
    except RuntimeError as e:
        print "Caught in loop from block '{}'".format(key)
        print e

    orphaned = [key for key in blocks if key not in visited]
    print "\n".join("Found orphaned block: " + key for key in orphaned)
    
        
def find_start(source, blocks, visited):

    for key, block in blocks.items():
        if key in visited:
            continue
        if isinstance(block["goto"], dict):
            if source in block["goto"].values():
                visited.add(key)
                find_start(key, blocks, visited)
        elif block["goto"] == source:
            visited.add(key)
            find_start(key, blocks, visited)
        else:
            pass#print "Unsupported type",type(block["goto"]),block["goto"]


def main():
    if not argv[1:]:
       check_file(r"C:\Users\Gary\Documents\Unity Projects\heros-journey\Assets\Resources\Fantasy.json")
    else:
        for f in argv[1:]:
            print("Analysing " + f)
            check_file(f)


if __name__ == "__main__":
    main()
    raw_input(">")
