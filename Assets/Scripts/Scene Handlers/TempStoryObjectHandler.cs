﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TempStoryObjectHandler : MonoBehaviour {

    GameManager gameManager;
    public StoryObject me;
    Transform parent;

    GameObject type;
    Text powerText;
    GameObject title;
    Button reroll;
    Text promptDisplay;
    Phrase phrase;

    bool powered = false;
    TooltipHolder currentTooltip;

    // Use this for initialization
    void Awake(){
        gameManager = FindObjectOfType<GameManager>();
        //Grab relevant gameObjects
        parent = this.gameObject.transform;
        int children = parent.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = parent.GetChild(i).gameObject;
            switch (child.name){
                case "Type":
                    type = child;
                    AddListeners(child);
                    break;
                case "Power":
                    AddListeners(child);
                    powerText = child.GetComponent<Text>();
                    break;
                case "Prompt":
                    promptDisplay = child.GetComponent<Text>();
                    break;
                case "Reroll":
                    reroll = child.GetComponent<Button>();
                    reroll.onClick.AddListener(Reroll);
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }

    void GeneratePhrase(){
        phrase = new Phrase();
        phrase.Roll(gameManager.templates.wordData, me.label);
        promptDisplay.text = phrase.ToString();
        me.details = phrase.ToString();
    }

    void Reroll(){
        me.Roll();
        GeneratePhrase();
        SetPowerText();
    }

    void AddListeners(GameObject gameObject){
        EventTrigger eventTrigger = gameObject.GetComponent<EventTrigger>();
        eventTrigger.AddListener(EventTriggerType.PointerEnter, SetTooltip);
        eventTrigger.AddListener(EventTriggerType.PointerExit, ClearTooltip);

    }

    void SetTooltip(PointerEventData eventData){
        currentTooltip = eventData.pointerCurrentRaycast.gameObject.GetComponent<TooltipHolder>();
        if (currentTooltip) currentTooltip.UpdateTooltip();
    }

    void ClearTooltip(PointerEventData eventData){
        if (currentTooltip != null) currentTooltip.ClearTooltip();
    }

    void Update(){
        if (Input.GetButton("Copy") &&
            (Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl))){
            GUIUtility.systemCopyBuffer = me.ToString();
        }
    }

    public void Init(string storyType) {
        me = gameManager.templates.templateObjects[storyType].Copy();
        me.Roll();
        powered = me.GetPower() > 0;
        powerText.gameObject.SetActive(powered);
        SetPowerText();

        type.GetComponent<Text>().text = me.label;
        type.GetComponent<TooltipHolder>().tooltip = me.tooltip;
        type.GetComponent<TooltipHolder>().cues = me.cues;
    }

    void SetPowerText(){
        powerText.text = "Power " + me.GetPower().ToString();
    }

}
