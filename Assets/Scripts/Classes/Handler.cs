﻿using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class Handler : MonoBehaviour {

    protected StoryManager storyManager;
    protected JSONParser jsonParser;
    public Passage passage;
    public TooltipHolder tooltip;
    public bool finished = false;

    public virtual void Awake() {
        storyManager = FindObjectOfType<StoryManager>();
        jsonParser = storyManager.jsonParser;
    }

    public virtual void Update(){
        if (finished){
            ReturnStory();
            finished = false;
        }
    }

    public void SetFinished(){
        finished = true;
    }

    public virtual void Init(bool newCard, Passage myPassage){
        passage = myPassage;
        //if (passage != null) Debug.Log("New handler Init" + passage.type);
        //else Debug.Log("New handler Init: NULL PASSAGE");
        //Debug.Break();
        if (passage != null) storyManager.CallBlock(passage.type);
        if (newCard) DataManager.SaveStory(storyManager.storyHolder);
    }

    public virtual void Init(bool newCard, Passage myPassage, Recordable recordable){
        Init(newCard, passage);
    }

    public abstract void ReturnStory();

    protected void AddListeners(GameObject gameObject){
        EventTrigger eventTrigger = gameObject.GetComponent<EventTrigger>();
        eventTrigger.AddListener(EventTriggerType.PointerEnter, SetTooltip);
        eventTrigger.AddListener(EventTriggerType.PointerExit, ClearTooltip);
    }

    void SetTooltip(PointerEventData eventData){
        //Debug.Log("Hit " + eventData.pointerCurrentRaycast.gameObject.name);
        tooltip = eventData.pointerCurrentRaycast.gameObject.GetComponent<TooltipHolder>();
        if (tooltip != null) tooltip.UpdateTooltip();
    }

    void ClearTooltip(PointerEventData eventData){
        if (tooltip != null) tooltip.ClearTooltip();
    }



}
