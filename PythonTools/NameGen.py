from pprint import pprint
from random import sample

VOWELS = "aeiouy" 
DATA = {
	"t": {x: 0 for x in "rtpldsn"}, 
	"s": {x: 0 for x in "bre,lou,la,li,lo,le,lu,mo,ma,mi,mu,ba,be,to,bo".split(",")}, 
	"m": {x: 0 for x in "won,mud,ber,pun,bin,won,bop,yin,gin".split(",")}, 
	"l": {x: 0 for x in "torn,yorn,elli,brood,boon,gort,mung,thorp,brat".split(",")}, 
        "TEMPLATES":  {x: 0 for x in ["slm", "sss", "sl", "mm", "msm","l'sl", "m-sl"]}
        }
TEST_COUNT = 1

##########
DATA = {'TEMPLATES': {"l'sl": 0,
               'm-sl': 2,
               'mm': -2,
               'msm': 1,
               'sl': 6,
               'slm': 3,
               'sss': 2},
 'l': {'boon': 0,
       'brat': 0,
       'brood': 1,
       'elli': 7,
       'gort': 0,
       'mung': 1,
       'thorp': 0,
       'torn': 0,
       'yorn': 2},
 'm': {'ber': 0,
       'bin': 0,
       'bop': 0,
       'gin': -1,
       'mud': 2,
       'pun': 4,
       'won': -1,
       'yin': -1},
 's': {'ba': 0,
       'be': 0,
       'bo': 2,
       'bre': 0,
       'la': 1,
       'le': 0,
       'li': 4,
       'lo': 0,
       'lou': 6,
       'lu': 1,
       'ma': 1,
       'mi': 0,
       'mo': 1,
       'mu': 0,
       'to': 2},
 't': {'d': 0, 'l': 2, 'n': 0, 'p': 0, 'r': 4, 's': 2, 't': 0}}
##########


def gen():
    keys = []
    template = sample(DATA["TEMPLATES"], 1)[0]
    keys.append(("TEMPLATES", template))
    pieces = [(c, sample(DATA[c],1)[0]) if c in DATA else (None, c) for c in template] 
    result = ""
    for parent, piece in pieces[:]:
        if result and result[-1] in VOWELS and piece[0] in VOWELS:
            letter = sample(DATA["t"], 1)[0]
            result += letter
            keys.append(("t", letter))
        if parent:
            keys.append((parent, piece))
        result += piece
    return result, keys


def test():
    result, keys = gen()
    print result.capitalize()
    vote = None
    while not vote:  
        try:
            vote = int(raw_input("Please vote: "))
        except ValueError:
            pass
    for parent, value in keys:
        DATA[parent][value] += vote


for i in range(TEST_COUNT):
    print i,"/",TEST_COUNT 
    test()
    
pprint(DATA)
