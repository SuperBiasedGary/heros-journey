﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuHandler : MonoBehaviour {

    public Button loadSave;
    public bool savedData = false;
    public bool warned = false;
    GameManager gameManager;
    public GameObject overlayPrefab;

    private void Awake(){
        StoryHolder storyHolder = DataManager.LoadStory();
        gameManager = FindObjectOfType<GameManager>();
        savedData = (storyHolder != null);
        loadSave.interactable = savedData;


        GameObject gameObject = GameObject.Find(overlayPrefab.name);
        if (gameObject == null){
            gameObject = Instantiate(overlayPrefab);
            gameObject.name = overlayPrefab.name;
            DontDestroyOnLoad(gameObject);
        }
        DontDestroyOnLoad(FindObjectOfType<EventSystem>().gameObject);
    }

    public void StartStory(){
        if (savedData && !warned){
            Debug.Log("Warning, this will erase current saved story.");
            gameManager.flowchart.ExecuteBlock("eraseSave");
            warned = true;
            return;
        }
        gameManager.ActivateStory();
    }

    public void LoadStory(){
        gameManager.LoadGame();
    }

    public void StorySettings(){
        SceneManager.LoadScene("StorySettings");
    }

    public void PromptGenerator(){
        SceneManager.LoadScene("PromptGenerator");
    }
}
