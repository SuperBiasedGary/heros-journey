﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PromptMenuHandler : MonoBehaviour {

    public GameObject buttons;
    public GameObject card;

    public void ReturnToMainMenu(){
        SceneManager.LoadScene("MainMenu");
    }

    public void InitCard(string cardType){
        buttons.SetActive(false);
        card.SetActive(true);
        card.GetComponent<TempStoryObjectHandler>().Init(cardType);
    }

    public void CloseCard(){
        buttons.SetActive(true);
        card.SetActive(false);
    }
}
