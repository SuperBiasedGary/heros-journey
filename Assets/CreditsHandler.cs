﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsHandler : MonoBehaviour {
	public void OpenUrl (string url) {
        Application.OpenURL(url);
	}
}
