﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class Recordable {

    public string prefabName;
    public string name;
    public abstract string ToHtml();

    protected string Reformat(string text){
        if (text == null) return "";
        return text.Replace("\n", "\n<br>");
    }

    protected string AddTag(string text, string tag){
        return AddTag(text, tag, false);
    }

    protected string AddTag(string text, string tag, bool skip_format){
        if (skip_format) return "<" + tag + ">" + text + "</" + tag + ">";
        else return "<" + tag + ">" + Reformat(text) + "</" + tag + ">";
    }

    protected string MakeBold(string text){
        return AddTag(text, "b");
    }
    protected string MakeItalic(string text){
        return AddTag(text, "i");
    }
    protected string MakeUnderlined(string text){
        return AddTag(text, "u");
    }
    protected string MakeStrikethrough(string text){
        return AddTag(text, "s");
    }
    protected string MakeParagraph(string text){
        return AddTag(text, "p");
    }


    protected string MakeHeader(string text, int level){
        return AddTag(text, "h" + level.ToString());
    }

    protected string MakeList(string[] texts, bool ordered){
        string result = "\n";
        foreach (string text in texts){
            if (text == "" || text == null) continue;
            result += AddTag(text, "li") + "\n";
        }
        if (ordered) return AddTag(result, "ol", true);
        else return AddTag(result, "ul", true);
    }


    protected string MakeObjList(List<Recordable> texts, bool ordered){
        string result = "\n";
        foreach (Recordable obj in texts){
            result += AddTag(obj.ToHtml(), "li") + "\n";
        }
        if (ordered) return AddTag(result, "ol", true);
        else return AddTag(result, "ul", true);
    }

}
