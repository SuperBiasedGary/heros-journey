﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Prompt: Recordable {

    public string label;
    public string tooltip;
    public string cues;

    public override string ToHtml(){
        return Reformat(label);
    }

    public override string ToString(){
        return label;
    }

    public Prompt Copy(){
        Prompt prompt = new Prompt();
        if (label != null) prompt.label = label;
        if (tooltip != null) prompt.tooltip = tooltip;
        if (cues != null) prompt.cues = cues;
        return prompt;
    }
}
