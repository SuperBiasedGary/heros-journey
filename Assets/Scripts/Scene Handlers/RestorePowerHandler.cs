﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestorePowerHandler : Handler {

    private PassageEndHandler endHandler;
    private Report report;

    private Text preamble;
    private Button startButton;
    private StoryObject target;
    private List<Button> cardButtons = new List<Button>();

    public override void Awake(){
        base.Awake();
        int children = transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Preamble":
                    preamble = child.GetComponent<Text>();
                    AddListeners(child);
                    break;
                case "Start":
                    startButton = child.GetComponent<Button>();
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }

    public override void Init(bool newCard, Passage passage, Recordable recordable){
        base.Init(newCard, passage);
        if (newCard){
            report = new Report();
            startButton.onClick.AddListener(FinishPowerRestore);
            cardButtons = storyManager.InitialiseButtons(passage.applicableTypes, "RESTORE");
            StoryManager.selectDelegate += ChooseTrainee;
            if (cardButtons.Count == 1){
                cardButtons[0].onClick.Invoke();
            } else if (cardButtons.Count == 0) {
                EndPremature();
            }
        } else {
            this.passage = passage;
            report = recordable as Report;
            startButton.gameObject.SetActive(false);
            endHandler = storyManager.AddPassageEnd();
            endHandler.SetText(report.result);
            endHandler.DisableButton(true);
            target = report.chosenStoryObject;
        }
        CalculatePreamble();
    }

    private void CalculatePreamble(){
        if (target != null) {
            preamble.text = "Restore power to " + target.GetFullName();
        } else {
            if (report.applicableTypestring == null){
                report.applicableTypestring = passage.applicableTypeString("/");
            }
            preamble.text = "Restore lost power to a " + report.applicableTypestring;
        }
    }

    void FinishPowerRestore() {
        if (target == null) return;
        RestorePower(1);
    }

    void RestorePower(int power){
        startButton.gameObject.SetActive(false);
        string result;
        if (power > 0) {
            target.AdjustPower(power);
            result = "Restored " + power.ToString() + " Power to " + target.GetName() + ", up to " + target.GetPower().ToString();
        } else {
            result = "Found nothing to restore power to.";
        }

        endHandler = storyManager.AddPassageEnd();
        endHandler.SetText(result);
        endHandler.AddListener(ReturnStory);
    }

    void EndPremature(){
        RestorePower(0);
    }

    void ChooseTrainee(){
        StoryObject choice = storyManager.selectedObject;

        if (!passage.isApplicableType(choice.label)){
            storyManager.CallBlock("wrongTypeRestore");
            return;
        }
        if (!choice.HasLostPower()){
            storyManager.CallBlock("fullPowerRestore");
            return;
        }
        target = choice;
        CalculatePreamble();
    }

    public override void ReturnStory(){
        storyManager.ClearCardButtons(cardButtons);
        report.prefabName = "restore_power";
        report.chosenStoryObject = target;
        endHandler.DisableButton(true);
        storyManager.storyHolder.story.Add(report);
        storyManager.FinishScene();
    }
}
