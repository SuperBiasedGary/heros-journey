﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class GameSettings {
    public bool rerollsEnabled = true;
    public bool tutorials = true;
    public bool tooltipDefaultOn = true;
    public KeyCode tooltipKeycode = KeyCode.LeftControl;
    public string exportFolder = "";

    public void initExportFolder(){
        string base_folder = "Base";
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer ||
            Application.platform == RuntimePlatform.LinuxPlayer)
        {
            base_folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            base_folder = Application.persistentDataPath;
            if (File.Exists(base_folder)) base_folder = Directory.GetParent(base_folder).ToString();
        }
        else
        {
            Debug.LogWarning("No folder set for " + Application.platform.ToString());
        }
        exportFolder = Path.Combine(base_folder, "Writing Desk Stories");
    }

}
