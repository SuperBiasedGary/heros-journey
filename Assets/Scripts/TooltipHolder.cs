﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TooltipHolder : MonoBehaviour {
    public string tooltip;
    public string cues;
    public StoryManager storyManager;
    public GameManager gameManager;
    private Button myButton;

    private void Awake(){
        storyManager = FindObjectOfType<StoryManager>();
        gameManager = FindObjectOfType<GameManager>();
        //if (GetComponent<Button>()) return;
        EventTrigger eventTrigger = GetComponent<EventTrigger>();
        eventTrigger.AddListener(EventTriggerType.PointerClick, OnClick);
        myButton = GetComponent<Button>();
    }

    public void ClearTooltip(){
        UpdateTooltip("", "");
    }


    public void UpdateTooltip(){
        UpdateTooltip(tooltip, cues);
    }

    public void UpdateTooltip(string explicitTooltip, string explicitCues){
        gameManager.tooltip.text = explicitTooltip;
        gameManager.cues.text = explicitCues;
    }

    void OnClick(PointerEventData eventData) {
        Transform parentT = transform.parent;
        if (!parentT) return;
        GameObject parent = parentT.gameObject;
        Button button = parent.GetComponent<Button>();
        while (button == null && parent != null){
            parentT = parent.transform.parent;
            if (!parentT) return;
            parent = parentT.gameObject;
            button = parent.GetComponent<Button>();
        }
        if (button) button.onClick.Invoke();
        if (myButton) myButton.onClick.Invoke();
    }
}
