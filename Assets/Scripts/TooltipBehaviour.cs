﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooltipBehaviour : MonoBehaviour {

    public Vector3 visible;
    public Vector3 offscreen;

    public Text tooltipText;
    public Text tooltipTextFull;
    public Text cueText;

    public GameObject tooltipHeader;
    public GameObject tooltipPanel;
    public GameObject tooltipPanelFull;
    public GameObject cuePanel;
    public GameObject cueHeader;

    public RectTransform rectTransform;
    private GameSettings gameSettings;

    private void Awake(){
        GameManager gameManager = FindObjectOfType<GameManager>();
        gameSettings = gameManager.gameSettings;
        gameManager.tooltip = tooltipText;
        gameManager.cues = cueText;
        rectTransform.anchoredPosition = offscreen;
    }

    void Update () {
        transform.SetAsLastSibling();
        //transform.position = Input.mousePosition + tooltipOffset;

        bool Keypressed = Input.GetKey(gameSettings.tooltipKeycode);
        bool active = (gameSettings.tooltipDefaultOn && !Keypressed)
                       || (!gameSettings.tooltipDefaultOn && Keypressed);
        bool hasTooltip = tooltipText.text != "";
        bool hasCue = cueText.text != "";

        if (!active || (!hasTooltip && !hasCue)) {
            tooltipText.text = "";
            cueText.text = "";
            rectTransform.anchoredPosition = offscreen;
        } else {
            tooltipTextFull.text = tooltipText.text;
            rectTransform.anchoredPosition = visible;
        }

        tooltipHeader.SetActive(hasTooltip);
        tooltipPanel.SetActive(hasTooltip && hasCue);
        tooltipPanelFull.SetActive(hasTooltip && !hasCue);
        cueHeader.SetActive(hasCue);
        cuePanel.SetActive(hasCue);
    }
}
