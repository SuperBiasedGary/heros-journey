﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RetirementCheckHandler : Handler {

    public override void Awake(){
        base.Awake();
    }

    public override void Init(bool newCard, Passage passage){
        base.Init(newCard, passage);
        if (!newCard) return;

        string check = passage.objectTypeFunction;
        string type = passage.objectType;
        bool result = Check(type, check);
        if (result) storyManager.storyHolder.blockFlag = "true";
        else storyManager.storyHolder.blockFlag = "false";
        ReturnStory();
    }

    public bool Check(string type, string check) {
        List<StoryObject> storyObjects = storyManager.storyHolder.GetAll(type);
        switch (check){
            case "ANY":
                foreach (StoryObject storyObject in storyObjects){
                    if (storyObject.retired) return true;
                }
                return false;
            case "ALL":
                foreach (StoryObject storyObject in storyObjects){
                    if (!storyObject.retired) return false;
                }
                return true;
            case "RECENT":
                StoryObject recent = storyManager.storyHolder.GetRecent(type);
                if (recent == null) return false;
                return recent.retired;
            case "LAST":
                return storyObjects[storyObjects.Count - 1].retired;
            case "FIRST":
                return storyObjects[0].retired;
            default:
                Debug.Log("No case found for " + check);
                return false;
        }
    }

    public override void ReturnStory(){
        storyManager.FinishScene();
    }
}
