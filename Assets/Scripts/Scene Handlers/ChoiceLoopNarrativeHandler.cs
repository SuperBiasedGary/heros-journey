﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoiceLoopNarrativeHandler : ChoiceNarrativeHandler
{
    private Button endButton;

    public override void Awake(){
        base.Awake();
        int children = transform.childCount;
        GameObject child;
        Button button;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            if (child.name.StartsWith("Choice")){
                button = child.GetComponent<Button>();
                button.onClick.AddListener(DeactivateEndButton);
            } else if (child.name == "EndChoice") { 
                endButton = child.GetComponent<Button>();
                endButton.onClick.AddListener(EndPassage);
                AddListeners(child.transform.GetChild(0).gameObject);
            }
        }
    }

    public void DeactivateEndButton(){
        endButton.gameObject.SetActive(false);
    }


    public override void Init(bool newCard, Passage passage){
        base.Init(newCard, passage);
        passage.prefabName = "choose_narrative_loop";
		// TODO? Add code for when not a newCard?
    }

    public override void ReturnStory(){
        RecordPassage();
        endButton.gameObject.SetActive(false);
        storyManager.RepeatScene();
    }


    public void EndPassage(){
        Destroy(this.gameObject);
        storyManager.FinishScene();
    }
}
