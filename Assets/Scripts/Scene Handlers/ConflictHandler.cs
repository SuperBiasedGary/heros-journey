﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConflictHandler : Handler
{
    private Text preamble;
    private Button submit;
    private PassageEndHandler endHandler;

    private StoryObject player;
    private StoryObject opponent;
    private GameObject startButton;
    public GameObject conflictEndPrefab;

    private StoryObject chosenAssistant;
    private StoryObject opponentAssistant;
    private List<Button> cardButtons = new List<Button>();
    private int assistantPower = 0;
    private int opponentAssistantPower = 0;

    int playerRoll = 0;
    int opponentRoll = 0;
    float flipTime = 1f;
    public Report report;

    public override void Awake(){
        base.Awake();
        int children = transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Preamble":
                    preamble = child.GetComponent<Text>();
                    AddListeners(child);
                    break;
                case "Start":
                    startButton = child;
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }


    public override void Init(bool newCard, Passage passage, Recordable recordable){
        base.Init(newCard, passage);

        if (newCard){
            report = new Report();
            startButton.GetComponent<Button>().onClick.AddListener(CalculateConflict);

            if (passage.assistType != "") {
                cardButtons = storyManager.InitialiseButtons(new string[] { passage.assistType }, "RETIRE_CHECK");
                StoryManager.selectDelegate += ChooseAssistant;
            }

            player = storyManager.storyHolder.GetStoryObject(passage.objectType, passage.objectTypeFunction);
            opponent = storyManager.storyHolder.GetStoryObject(passage.opponentType, passage.opponentTypeFunction);
            storyManager.storyHolder.SetRecent(opponent);
            ChooseOpponentAssistant();
            preamble.text = CalculatePreamble();
        } else {
            report = recordable as Report;
            preamble.text = report.header;
            startButton.SetActive(false);
            int length = report.LongestFlip();
            string[] strings;
            for (int i = 0; i < length; i++){
                strings = report.GetFlips(i);
                storyManager.AddDoubleTextScrap(strings[0], strings[1]);
            }
            PassageEndHandler handler = storyManager.AddPassageEnd();
            handler.SetText(report.result);
            handler.DisableButton(true);
        }
    }

    void ChooseAssistant(){
        if (storyManager.selectedObject == null) return;
        StoryObject choice = storyManager.selectedObject;
        if (choice.label != passage.assistType){
            storyManager.CallBlock("wrongTypeConflict");
            return;
        }
        chosenAssistant = choice;
        // Recalculate header
        preamble.text = CalculatePreamble();
    }

    void ChooseOpponentAssistant(){
        Debug.Log("Getting all active " + passage.opponentAssistType);
        List<StoryObject> options = storyManager.storyHolder.GetStoryObjects(passage.opponentAssistType, "ACTIVE");

        if (options == null || options.Count == 0) return;
        opponentAssistant = options[UnityEngine.Random.Range(0, options.Count)];
        
        // Recalculate header
        preamble.text = CalculatePreamble();
    }

    private string CalculatePreamble(){
        string preamble = "Conflict between ";
        preamble += player != null ? player.GetName() : "???";
        preamble += " and " + (opponent != null ? opponent.GetName() : "???");
        if (opponentAssistant != null) {
            preamble += " assisted by " + opponentAssistant.GetName();
        }

        if (chosenAssistant != null) {
            preamble += "\nWith " + chosenAssistant.GetName() + " assisting.";
        } else if (storyManager.storyHolder.IsAvailable(passage.assistType)){
            preamble += "\nOptional: Choose a " + passage.assistType + " to aid " + player.GetName() + ".\nBut remember, a participating " + passage.assistType + " ALWAYS loses 1 power, regardless of outcome.";
        }
        return preamble;
    }

    private void CalculateConflict() {
        Destroy(startButton);
        // Remove the assistant note
        if (chosenAssistant == null) preamble.text = CalculatePreamble();
        report.header = preamble.text;
        storyManager.AddDoubleTextScrap(player.GetName() + " Results:", opponent.GetName() + " Results:");
        storyManager.storyHolder.SetRecent(chosenAssistant);
        storyManager.storyHolder.SetRecent(opponentAssistant);
        storyManager.ClearCardButtons(cardButtons);
        StartCoroutine(Flip());
    }

    private bool CriticalCheck(){
        // False if a tie
        if (playerRoll == opponentRoll) return false;
        // Player Win
        if (playerRoll > opponentRoll) {
            // False if only 1 coin was flipped
            if (player.GetPower() + assistantPower < 2) return false;
            // False if opponent has only 1 power
            if (opponent.GetPower() < 2) return false;
            // All heads on player side, all tails on opponent side
            return playerRoll == (player.GetPower() + assistantPower) && opponentRoll == 0;
        }
        // Opponent Win
        else if (opponentRoll > playerRoll) {
            // False if only 1 coin was flipped
            if (opponent.GetPower() < 2) return false;
            // False if player has only 1 power
            if (player.GetPower() < 2) return false;
            // All heads on opponent side, all tails on player side
            return opponentRoll == opponent.GetPower() && playerRoll == 0;
        }
        Debug.Log("Unexpected branch reached in CriticalCheck, returning false");
        return false;
    }

    private void CalculateResult() {
        endHandler = storyManager.AddPassageEnd();
        endHandler.AddListener(ReturnStory);

        bool critical = CriticalCheck();

        if (playerRoll > opponentRoll){
            storyManager.storyHolder.blockFlag = "WIN";
            if (critical){
                report.result = player.GetName() + " won with a critical success! -2 power!";
                ChangePower(-2, opponent);
            } else {
                report.result = player.GetName() + " won!";
                ChangePower(-1, opponent);
            }
        } else if(opponentRoll > playerRoll){
            storyManager.storyHolder.blockFlag = "LOSS";
            if (critical) {
                report.result = player.GetName() + " lost with a critical failure! -2 power!";
                ChangePower(-2, player);
            } else {
                report.result = player.GetName() + " lost!";
                ChangePower(-1, player);
            }
        } else {
            report.result = player.GetName() + " and " + opponent.GetName() + " tied!";
            ChangePower(-1, player);
            ChangePower(-1, opponent);
            storyManager.storyHolder.blockFlag = "TIE";
        }
        if (chosenAssistant != null) ChangePower(-1, chosenAssistant);
        if (opponentAssistant != null) ChangePower(-1, opponentAssistant);
        endHandler.SetText(report.result);
    }

    private void ChangePower(int change, StoryObject storyObject){
        storyObject.AdjustPower(change);
        if (storyObject.retired){
            report.result += "\n" + storyObject.GetName() + " loses power and has been retired!";
        } else {
            report.result += "\n" + storyObject.GetName() + " loses power.";
        }
    }

    IEnumerator Flip() {
        if (chosenAssistant != null) assistantPower = chosenAssistant.GetPower();
        if (opponentAssistant != null) opponentAssistantPower = opponentAssistant.GetPower();

        string playerKey = player.GetFullName();
        string opponentKey = opponent.GetFullName();
        int max = Mathf.Max(player.GetPower() + assistantPower, opponent.GetPower() + opponentAssistantPower);
        report.flips[playerKey] = new string[player.GetPower() + assistantPower];
        report.flips[opponentKey] = new string[opponent.GetPower() + opponentAssistantPower];
        string flip;
        string[] display = new string[2];
        for(int progress = 0; progress < max; progress++) { 
            yield return new WaitForSeconds(flipTime);
            display[0] = "";
            display[1] = "";
            //Player
            if (progress < player.GetPower()){
                if (UnityEngine.Random.value > 0.5){
                    playerRoll++;
                    flip = "HEADS";
                } else {
                    flip = "TAILS";
                }
                display[0] = flip;
                report.flips[playerKey][progress] = flip;
            }
            else if (assistantPower > 0 && progress < player.GetPower() + chosenAssistant.GetPower()) {
                if (UnityEngine.Random.value > 0.5){
                    playerRoll++;
                    flip = "[" + chosenAssistant.GetName() + "] HEADS";
                }
                else {
                    flip = "[" + chosenAssistant.GetName() + "] TAILS";
                }
                display[0] = flip;
                report.flips[playerKey][progress] = flip;
            }
            //Opponent
            if (progress < opponent.GetPower()){
                if (UnityEngine.Random.value > 0.5){
                    opponentRoll++;
                    flip = "HEADS";
                } else {
                    flip = "TAILS";
                }
                display[1] = flip;
                report.flips[opponentKey][progress] = flip;
            } else if (opponentAssistantPower > 0 && progress < opponent.GetPower() + opponentAssistantPower) {
                if (UnityEngine.Random.value > 0.5){
                    opponentRoll++;
                    flip = "[" + opponentAssistant.GetName() + "] HEADS";
                } else {
                    flip = "[" + opponentAssistant.GetName() + "] TAILS";
                }
                display[1] = flip;
                report.flips[opponentKey][progress] = flip;
            }
            storyManager.AddDoubleTextScrap(display[0], display[1]);
        }
        yield return new WaitForSeconds(flipTime/2);
        CalculateResult();
    }

    public override void ReturnStory(){
        report.prefabName = "conflict";
        endHandler.DisableButton(true);
        storyManager.storyHolder.story.Add(report);
        storyManager.FinishScene();
    }
}
