﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PassageEndHandler : MonoBehaviour {

    private Text text;
    private Button button;

	void Awake () {
        int children = transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Result":
                    text = child.GetComponent<Text>();
                    break;
                case "Continue":
                    button = child.GetComponent<Button>();
                    break;
            }
        }
    }

    public void SetText(string value){
        text.text = value;
    }

    public void AddListener(UnityAction call){
        button.onClick.AddListener(call);
    }

    public void DisableButton(bool delete){
        button.onClick.RemoveAllListeners();
        if (delete) Destroy(button.gameObject);
    }
}
