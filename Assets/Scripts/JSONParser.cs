﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class JSONParser : MonoBehaviour {

    public JSONNode storyObjectData;
    public JSONNode passageData;
    public JSONNode wordData;
    public JSONNode nameData;
    public string filename;


    JSONNode LoadFile(string filename){
        string path;
        if (Application.platform == RuntimePlatform.Android){
            // https://answers.unity.com/questions/1231211/how-to-get-resources-filepath-on-android-device.html
            string tempPath = Path.Combine(Application.streamingAssetsPath, filename);
            WWW reader = new WWW(tempPath);
            while (!reader.isDone) {}
            return JSON.Parse(reader.text);
        } else {
            path = Path.Combine("file://" + Application.dataPath, "StreamingAssets");
        }
        path = Path.Combine(path, filename);
        WWW www = new WWW(path);
        return JSON.Parse(www.text);
    }

    private void Awake(){
        GameManager gameManager = GetComponent<GameManager>();
        Templates templates = gameManager.templates;

        JSONNode loadedData = LoadFile(filename);
        templates.storyStructure = loadedData["Blocks"];
        storyObjectData = loadedData["Templates"];
        passageData = loadedData["Prompts"];
        wordData = LoadFile("words.json");
        nameData = LoadFile("names.json");
        templates.wordData = wordData;
        templates.nameData = nameData;

        StoryObject template;
        string value;
        templates.templateObjects = new Dictionary<string, StoryObject>();
        foreach (JSONNode key in storyObjectData.Keys){
            value = key.Value;
            template = DefineTemplateObject(value);
            templates.templateObjects[value] = template;
        }

        Passage passage;
        templates.templatePassages = new Dictionary<string, Passage>();
        foreach (JSONNode key in passageData.Keys){
            value = key.Value;
            passage = DefinePassageTemplate(value);
            templates.templatePassages[value] = passage;
        }
    }

    public StoryObject DefineTemplateObject(string key){
        StoryObject storyObject = new StoryObject();
        JSONNode values = storyObjectData[key];
        storyObject.label = key;
        storyObject.tooltip = (string)values["tooltip"];
        storyObject.cues = (string)values["cues"];
        storyObject.named = (bool) values["Named"];

        if (values["Power"] != null){
            storyObject.powerRanges = new List<int[]>();
            JSONNode thisRange;
            for (int i = 0; i < values["Power"].Count; i++){
                thisRange = values["Power"][i];
                storyObject.powerRanges.Add(new int[thisRange.Count]);
                for (int j = 0; j < thisRange.Count; j++){
                    storyObject.powerRanges[i][j] = thisRange[j];
                }
            }
        }
        return storyObject;
    }

    public Field DefineTemplateField(JSONNode fieldData){
        Field field = new Field();
        field.name = fieldData["name"];
        field.tooltip = fieldData["tooltip"];
        field.cues = fieldData["cues"];
        field.options = new Prompt[fieldData["prompts"].Count];

        int x = 0;
        foreach (JSONNode option in fieldData["prompts"].Values){
            field.options[x] = DefinePrompt(option);
            x++;
        }
        return field;
    }

    private StoryObject DefineStoryObject(string label){
        StoryObject storyObject = new StoryObject();
        JSONNode values = storyObjectData[label];
        storyObject.label = label;
        storyObject.tooltip = (string)values["tooltip"];
        storyObject.cues = (string)values["cues"];
        storyObject.named = (bool)values["Named"];
        if(values["Power"] != null){
            int choice = UnityEngine.Random.Range(0, values["Power"].Count - 1);
            for (int j = 0; j <= choice; j++){
                storyObject.SetStartingPower(values["Power"][j]);
            }
        }
        return storyObject;
    }

    public Field DefineField(JSONNode fieldData){
        Field field = new Field();
        field.name = fieldData["name"];
        field.tooltip = fieldData["tooltip"];
        field.cues = fieldData["cues"];
        field.options = new Prompt[fieldData["prompts"].Count];

        int x = 0;
        foreach (JSONNode option in fieldData["prompts"].Values){
            field.options[x] = DefinePrompt(option);
            x++;
        }

        field.chosenPrompt = field.options[UnityEngine.Random.Range(0, field.options.Length - 1)];
        return field;
    }

    public Passage DefinePassageTemplate(string label){
        Passage passage = new Passage();
        JSONNode data = passageData[label];
        passage.label = label;
        passage.type = (string) data["type"];
        passage.objectType = (string)data["object_type"];
        passage.objectTypeFunction = (string)data["object_type_function"];
        passage.tooltip = (string)data["tooltip"];
        passage.cues = (string)data["cues"];
        passage.modifier = (int)data["modifier"];
        passage.minimumChoices = (int)data["minimum_choices"];
        passage.tutorialOnly = (bool)data["tutorial_only"];
        passage.definitionNeeded = (bool)data["definition_needed"];

        passage.opponentType = (string)data["opponent_type"];
        passage.opponentTypeFunction = (string)data["opponent_type_function"];
        passage.assistType = (string)data["assist_type"];
        passage.assistFunction = (string)data["assist_function"];
        passage.opponentAssistType = (string)data["opponent_assist_type"];
        passage.opponentAssistFunction = (string)data["opponent_assist_function"];

        int typeCount = (int)data["applicable_types"].Count;
        passage.applicableTypes = new string[typeCount];
        if (typeCount > 0){
            int x = 0;
            foreach (JSONNode typeName in data["applicable_types"].Values){
                passage.applicableTypes[x] = typeName.Value;
                x++;
            }
        }

        int promptCount = (int)data["options"].Count;
        passage.prompts = new Prompt[promptCount];
        if (promptCount > 0){
            int x = 0;
            foreach (JSONNode prompt in data["options"].Values){
                passage.prompts[x] = DefinePrompt(prompt);
                x++;
            }
        }

        int choiceCount = (int)data["choices"].Count;
        passage.choices = new Choice[choiceCount];
        if (choiceCount > 0){
            int x = 0;
            foreach (JSONNode choice in data["choices"].Values){
                passage.choices[x] = DefineChoice(choice);
                x++;
            }
        }
        return passage;
    }

    public Prompt DefinePrompt(JSONNode promptData){
        Prompt prompt = new Prompt();
        prompt.label = (string)promptData["label"];
        prompt.tooltip = (string)promptData["tooltip"];
        prompt.cues = (string)promptData["cues"];
        return prompt;
    }

    public Choice DefineChoice(JSONNode choiceData){
        Choice choice = new Choice();

        choice.label = choiceData["label"];
        choice.flag = choiceData["flag"];
        choice.tooltip = choiceData["tooltip"];
        choice.cues = choiceData["cues"];
        choice.weight = choiceData["weight"];

        choice.function = (string)choiceData["function"];
        choice.requirement = (string)choiceData["requirement"];
        choice.selector = (string)choiceData["selector"];
        choice.applicableTypes = new string[choiceData["applicable_types"].Count];
        int i = 0;
        foreach (string typename in choiceData["applicable_types"].Values){
            choice.applicableTypes[i] = typename;
            i++;
        }
        choice.modifierName = choiceData["modifier_name"];
        choice.modifierOperation = choiceData["modifier_operation"];

        if (choiceData["always_active"]) choice.active = true;
        return choice;
    }
}
