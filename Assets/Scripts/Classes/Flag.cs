﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Flag : Recordable {
    public string[] flags;

    public override string ToString(){
        if (flags.Length == 0) return "";

        string result = "Flagged Events\n";
        foreach (string flag in flags) result += flag + "\n";
        return result;
    }
    public override string ToHtml(){
        if (flags.Length == 0) return "";

        string result = MakeHeader("Flagged Events", 5) + "\n";
        return result + MakeList(flags, false);
    }
}
