﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EpilogueHandler : Handler
{
    //public GameObject epiloguePrefab;
    public GameObject textEntryPrefab;

    private GameObject passageUI;
    private InputField textEntry;
    private Text textDisplay;
    private Button finish;
    private Button cancel;
    private Button submit;

    private StoryObject epiloguing;
    private List<Button> cardButtons;

    public override void Awake()
    {
        base.Awake();
        int children = transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++)
        {
            child = transform.GetChild(i).gameObject;
            switch (child.name)
            {
                case "Type":
                    child.GetComponent<Text>().text = passage.label;
                    child.GetComponent<TooltipHolder>().tooltip = passage.tooltip;
                    child.GetComponent<TooltipHolder>().cues = passage.cues;
                    AddListeners(child);
                    break;
                case "Finish":
                    finish = child.GetComponent<Button>();
                    finish.onClick.AddListener(ReturnStory);
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }

    public override void Init(bool newCard, Passage passage, Recordable epilogue) {
        Init(newCard, epilogue as Passage);
    }
    public override void Init(bool newCard, Passage passage)
    {
        base.Init(newCard, passage);
        if (newCard){
            RebuildCardButtons();
        } else {
            EpiloguePassage epilogue = passage as EpiloguePassage;
            LoadEpilogue(epilogue);
            Destroy(transform.gameObject);
        }
    }

    private void RebuildCardButtons(){
        string[] types = new string[] { "Hero", "Antagonist", "Setting", "Quest", "Ally", "Obstacle" };
        cardButtons = storyManager.InitialiseButtons(types, "EPILOGUE");
        if (cardButtons.Count < 1){
            ReturnStory();
            return;
        }
        StoryManager.selectDelegate += ChooseEpilogue;
    }

    private string GetTitle(){
        return "Epilogue for " + epiloguing.GetFullName();
    }

    private void ChooseEpilogue(){
        if (storyManager.selectedObject == null) return;
        finish.gameObject.SetActive(false);
        epiloguing = storyManager.selectedObject;

        passageUI = storyManager.AddUI(textEntryPrefab);
        int children = passageUI.transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = passageUI.transform.GetChild(i).gameObject;
            switch (child.name){
                case "StoryObject":
                    child.GetComponent<Text>().text = GetTitle();
                    break;
                case "Passage Field":
                    textEntry = child.GetComponent<InputField>();
                    break;
                case "Passage Text":
                    textDisplay = child.GetComponent<Text>();
                    break;
                case "Cancel":
                    cancel = child.GetComponent<Button>();
                    cancel.onClick.AddListener(CancelEpilogue);
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
                case "Submit":
                    submit = child.GetComponent<Button>();
                    submit.onClick.AddListener(AddStory);
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }

    private void LoadEpilogue(EpiloguePassage passage){
        finish.gameObject.SetActive(false);

        passageUI = storyManager.AddUI(textEntryPrefab);
        int children = passageUI.transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = passageUI.transform.GetChild(i).gameObject;
            switch (child.name){
                case "StoryObject":
                    child.GetComponent<Text>().text = passage.label;
                    break;
                case "Passage Field":
                    child.SetActive(false);
                    break;
                case "Passage Text":
                    child.SetActive(true);
                    child.GetComponent<Text>().text = passage.text;
                    break;
                case "Cancel":
                    child.SetActive(false);
                    break;
                case "Submit":
                    child.SetActive(false);
                    break;
            }
        }
    }

    private void CancelEpilogue(){
        epiloguing = null;
        Destroy(passageUI);
        finish.gameObject.SetActive(true);
        RebuildCardButtons();
    }

    public void AddStory(){
        if (textEntry.text == ""){
            storyManager.CallBlock("missingParagraph");
            return;
        }

        EpiloguePassage passage = new EpiloguePassage();
        passage.storyObject = epiloguing;
        passage.label = GetTitle();
        passage.text = textEntry.text;
        passage.prefabName = "epilogue";
        textDisplay.text = textEntry.text;
        storyManager.storyHolder.story.Add(passage);

        // Reconfiguring
        epiloguing.epilogued = true;
        textDisplay.gameObject.SetActive(true);
        textEntry.gameObject.SetActive(false);
        submit.gameObject.SetActive(false);
        cancel.gameObject.SetActive(false);
        storyManager.RepeatScene();
    }

    public override void ReturnStory(){
        storyManager.FinishScene();
    }
}