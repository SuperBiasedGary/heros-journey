import os
import re

from collections import defaultdict, Counter
from pprint import pprint
from sys import argv
from copy import deepcopy

try:
    from simplejson import load, dump
except ImportError:
    from json import load, dump

PATTERN = re.compile(r"\[.*?\]")


def analyse_file(path):
    with open(path) as f:
        columns = f.readline().strip().split(",")[1:]
        data = map(str.strip, f.readlines())
    if not data:
        print("No data found")
        return
    firstline = data[0].split(",")
    firstline.pop(0)
    tags = [False] * len(firstline)
    for line in data:
        line = line.strip().split(",")[1:]
        for i, value in enumerate(line):
            if value.lower() == "x":
                tags[i] = True
    results = {}
    for line in data:
        line = line.strip(",").split(",")
        if not line:
            continue
        line_data = {"tags":[]}
        key = line.pop(0)
        for i, value in enumerate(line):
            value = value.strip('"')
            if not tags[i]:
                line_data[columns[i]] = value
            elif value:
                line_data["tags"].append(columns[i])
        results[key] = line_data
    return results


def analyse_phrase_file(path):
    with open(path) as f:
        columns = f.readline()
        data = map(str.strip, f.readlines())
    if not data:
        print("No data found")
        return
    results = defaultdict(list)
    for line in data:
        typename, phrase = line.split(",", 1)
        if not line:
            continue
        results[typename].append(phrase.strip('"'))
    return results
    


def main():
    
    data = {}
    for f in argv[1:]:
        print("Analysing " + f)
        basename = os.path.basename(f).replace(".csv", "")
        if "phrases" == basename.lower():
            continue
            file_data = analyse_phrase_file(f)
        else:
            file_data = analyse_file(f)
        data[basename] = file_data
    path = os.path.join(os.path.dirname(__file__), "output.json") 
    with open(path, "w") as f:
        dump(data, f, indent=4, sort_keys=True)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print e
    raw_input("Complete")
