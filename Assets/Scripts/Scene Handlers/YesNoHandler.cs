﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class YesNoHandler : Handler {
    Text preamble;
    Button yesButton;
    Button noButton;
    Button flipButton;
    GameObject[] choiceDisplays = new GameObject[3];
    GameObject[] strikethroughs = new GameObject[3];
    string chosenFlag;

    private const float WAIT_TIME = 1f;

    public override void Awake() {
        base.Awake();

        GameObject child;
        int index;
        for (int i = 0; i < transform.childCount; i++) {
            child = transform.GetChild(i).gameObject;
            int.TryParse(child.name.Substring(child.name.Length - 1), out index);
            if (child.name.StartsWith("TextChoice")) {
                choiceDisplays[index] = child;
            } else if (child.name.StartsWith("Strikethrough")) {
                strikethroughs[index] = child;
            } else {
                switch (child.name) {
                    case "Preamble":
                        AddListeners(child);
                        preamble = child.GetComponent<Text>();
                        break;
                    case "YesButton":
                        yesButton = child.GetComponent<Button>();
                        break;
                    case "NoButton":
                        noButton = child.GetComponent<Button>();
                        break;
                    case "FlipButton":
                        AddListeners(child.GetComponentInChildren<Text>().gameObject);
                        flipButton = child.GetComponent<Button>();
                        break;
                }
            }
        }
    }

    public override void Init(bool newCard, Passage passage) {
        base.Init(newCard, passage);
        preamble.text = passage.tooltip;
        if (newCard) {
            SetupButtons();
        } else {
            DeactivateButtons(passage.modifier);
            if (passage.text != "") storyManager.AddTextScrap(passage.text);
        }
    }


    void SetupButtons(){
        yesButton.onClick.AddListener(() => Choose(0, "YES"));
        noButton.onClick.AddListener(() => Choose(1, "NO"));
        flipButton.onClick.AddListener(() => Choose(2, ""));
    }

    void DeactivateButtons(int chosenNum){
        yesButton.gameObject.SetActive(false);
        noButton.gameObject.SetActive(false);
        flipButton.gameObject.SetActive(false);
        foreach (GameObject display in choiceDisplays) display.SetActive(true);
        for (int i = 0; i < strikethroughs.Length; i++){
            if (i != chosenNum) strikethroughs[i].SetActive(true);
        }
    }

    void Choose(int i, string flag){
        DeactivateButtons(i);
        passage.modifier = i;
        if (flag == "") {
        	StartCoroutine(Flip());
        } else {
        	chosenFlag = flag;
        	ReturnStory();
        }
    }

    IEnumerator Flip(){
        string result;
    	if (Random.Range(0, 1) > 0.5){
    		chosenFlag = "YES";
            result = "HEADS: The coin has chosen YES.";
    	} else {
    		chosenFlag = "NO";
            result = "TAILS: The coin has chosen NO.";
    	}
        yield return new WaitForSeconds(WAIT_TIME / 2);
        storyManager.AddTextScrap(result);
        passage.text = result;
        yield return new WaitForSeconds(WAIT_TIME);
        ReturnStory();
    }

    public override void ReturnStory(){
    	passage.prefabName = "yesno";
    	storyManager.storyHolder.story.Add(passage);
    	storyManager.storyHolder.blockFlag = chosenFlag;
    	storyManager.FinishScene();
    }
}
    