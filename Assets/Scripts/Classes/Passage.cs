﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Passage : Recordable {

    public string label;
    public string type;
    public string tooltip;
    public string cues;

    public Prompt[] prompts;
    public Prompt chosenPrompt;
    public Choice[] choices;
    public Choice chosenChoice;
    public int minimumChoices;

    public bool tutorialOnly = false;
    public bool definitionNeeded;
    public string[] applicableTypes;
    public string text = "";
    public int modifier;

    public string objectType;
    public string objectTypeFunction;
    public string assistType;
    public string assistFunction;
    public string opponentType;
    public string opponentTypeFunction;
    public string opponentAssistType;
    public string opponentAssistFunction;


    public override string ToString(){
        string result = label;
        if (chosenPrompt != null) result += ": " + chosenPrompt.label;
        result += "\n" + text;
        return result;
    }

    public override string ToHtml(){
        string result;
        if (chosenPrompt != null) result = MakeHeader(label + ": " + chosenPrompt.label, 2) + "\n";
        else result = MakeHeader(label, 2) + "\n";
        
        if (choices != null && choices.Length > 0){
            string choicelist = "";
            foreach(Choice choice in choices){
                //if (!choice.active) continue;
                if (choice == null) continue;
                Debug.Log(choice.label);
                if (chosenChoice != null && choice.label == chosenChoice.label){
                    choicelist += "\n" + AddTag(MakeBold(choice.label + " [CHOSEN]"), "li");
                } else {
                    choicelist += "\n" + AddTag(choice.label, "li");
                }
            }
            result += AddTag(choicelist, "ul");
        }

        if (text != "") result += "\n" + MakeParagraph(text);
        return result;
    }

    public string ToString(bool verbose){
        return ("label = " + label + "\n" +
                "type = " + type + "\n" +
                "tooltip = " + tooltip + "\n" +
                "cues = " + cues + "\n" +
                "prompts = " + prompts.ToString() + "\n" +
                "chosenPrompt = " + (chosenPrompt == null ? "null" : chosenPrompt.ToString()) + "\n" +
                "tutorialOnly = " + tutorialOnly.ToString() + "\n" +
                "definitionNeeded = " + definitionNeeded.ToString() + "\n" +
                "applicableTypes = " + applicableTypes.ToString() + "\n" +
                "objectType = " + objectType + "\n" +
                "objectTypeFunction = " + objectTypeFunction + "\n" +
                "text = " + text);
    }
    public void Roll(){
        if (prompts.Length == 0) return;
        int i = UnityEngine.Random.Range(0, prompts.Length - 1);
        chosenPrompt = prompts[i];
    }

    public Passage Copy(){
        Passage passage = new Passage();
        passage.label = label;
        passage.type = type;
        passage.tooltip = tooltip;
        passage.cues = cues;
        passage.prompts = prompts;
        passage.choices = choices;
        passage.minimumChoices = minimumChoices;
        passage.modifier = modifier;
        passage.tutorialOnly = tutorialOnly;
        passage.definitionNeeded = definitionNeeded;
        passage.objectType = objectType;
        passage.objectTypeFunction = objectTypeFunction;
        passage.applicableTypes = applicableTypes;
        passage.text = text;
        passage.opponentType = opponentType;
        passage.opponentTypeFunction = opponentTypeFunction;
        passage.assistType = assistType;
        passage.assistFunction = assistFunction;
        passage.opponentAssistType = opponentAssistType;
        passage.opponentAssistFunction = opponentAssistFunction;
        return passage;
    }

    public string applicableTypeString(string separator){
        string result = "";
        foreach(string typeName in applicableTypes){
            result += typeName + separator;
        }
        int index = result.LastIndexOf(separator);
        if (index > 0) result = result.Remove(index);
        return result;
    }


    public bool isApplicableType(string check){
        foreach (string typeName in applicableTypes){
            if (check == typeName) return true;
        }
        return false;
    }

}
