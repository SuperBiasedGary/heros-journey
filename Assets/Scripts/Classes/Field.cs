using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Field: Recordable {

    public string tooltip;
    public string cues;
    public bool primaryField = false;
    public Prompt[] options;
    public Prompt chosenPrompt;
    public List<Prompt> oldPrompts = new List<Prompt>();
    public bool mandatory;

    public void UpdatePrompt(Prompt prompt){
        oldPrompts.Add(chosenPrompt);
        chosenPrompt = prompt;
    }

    public override string ToHtml(){
        string result = "";
        foreach (Prompt prompt in oldPrompts){
            result += AddTag(MakeStrikethrough(name + ": " + prompt.ToHtml()), "li") + "\n";
        }
        if (chosenPrompt != null) result += AddTag(name + ": " + chosenPrompt.ToHtml(), "li") + "\n";
        else result += AddTag(name + ": [Nothing Chosen]", "li");
        return result;
    }

    public override string ToString(){
        string result = "";
        foreach (Prompt prompt in oldPrompts)
        {
            result += "OLD PROMPT: " + name + ": " + chosenPrompt.ToString() + "\n";
        }
        if (chosenPrompt != null) result += name + ": " + chosenPrompt.ToString();
        else result += name + ": [Nothing Chosen]";
        return result;
    }

    public string ToString(bool latestOnly){
        if (!latestOnly) return ToString();
        if (chosenPrompt != null) return name + ": " + chosenPrompt.ToString();
        return name + ": [Nothing Chosen]";
    }

    internal Field Copy(){
        Field field = new Field();
        field.name = name;
        field.tooltip = tooltip;
        field.cues = cues;
        field.options = new Prompt[options.Length];
        for (int i = 0; i < options.Length; i++){
            field.options[i] = options[i];
        }
        field.chosenPrompt = chosenPrompt.Copy();
        field.mandatory = mandatory;
        return field;
    }
}
