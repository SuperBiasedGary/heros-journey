﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StoryObject : Recordable {

    private string nameValue = "";

    public string label;
    public bool named;
    public string details;
    public bool retired = false;

    public List<int[]> powerRanges;
    private int power;
    public int startingPower;

    public string tooltip;
    public string cues;
    public bool epilogued = false;

    public void SetName(string name){
        nameValue = name;
    }


    public string GetName(){
        if (nameValue == "") return label;
        else return nameValue;
    }

    public string GetFullName(){
        if (nameValue == "") return label;
        else return label + ": " + nameValue;
    }


    public void AdjustPower(int change){
        power += change;
        if (power > 0) retired = false;
        else if (power <= 0){
            power = 0;
            retired = true;
        }
    }

    public int GetPower(){
        return power;
    }

    public void SetStartingPower(int power){
        this.power = power;
        startingPower = power;
    }
    

    public bool HasLostPower(){
        return power < startingPower;
    }

    public override string ToHtml(){
        string header = label;
        if (named) header += " - " + nameValue;
        if (retired) header += " - Retired";
        if (power > 0){
            if (startingPower != power){
                header += " - Power " + power.ToString() + "/" + startingPower.ToString();
            } else {
                header += " - Power " + power.ToString();
            }
        }
        string result = MakeHeader(header, 2) + "\n";
        return result + MakeParagraph(details);
    }

    public override string ToString(){
        string result = label;
        if (named) result += " - " + nameValue;
        if (retired) result += " - Retired";
        if (startingPower > 0) result += " - Power " + startingPower.ToString();
        return result + "\n" + details;
    }

    public string ToBasicString(){
        string result = label;
        if (power > 0) result += " - Power " + power.ToString();
        return result + "\n" + details;
    }

    public void Roll(){
        Roll(0);
    }

    public void Roll(int powerCategory){
        if (powerRanges != null){
            int selection = powerCategory;
            selection = Mathf.Max(selection, 0);
            selection = Mathf.Min(selection, powerRanges.Count);

            int[] powerRange = powerRanges[selection];
            power = powerRange[UnityEngine.Random.Range(0, powerRange.Length - 1)];
            startingPower = power;
        }
    }

    public StoryObject Copy(){
        StoryObject storyObject = new StoryObject();
        storyObject.label = label;
        storyObject.named = named;
        storyObject.powerRanges = powerRanges;
        storyObject.tooltip = tooltip;
        storyObject.cues = cues;
        return storyObject;
    }
}
