﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoiceNarrativeHandler : ChoiceHandler
{

    private Text typeDisplay;
    private Text promptDisplay;

    private InputField textEntry;
    private Text textDisplay;
    private Button submit;

    public override void Awake(){
        base.Awake();
        int children = transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Sample":
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
                case "Type":
                    typeDisplay = child.GetComponent<Text>();
                    AddListeners(child);
                    break;
                case "Prompt":
                    promptDisplay = child.GetComponent<Text>();
                    AddListeners(child);
                    break;
                case "Passage Field":
                    textEntry = child.GetComponent<InputField>();
                    AddListeners(child);
                    break;
                case "Passage Display":
                    textDisplay = child.GetComponent<Text>();
                    break;
                case "Submit":
                    submit = child.GetComponent<Button>();
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }

    public override void Init(bool newCard, Passage passage){
        base.Init(newCard, passage);
        passage.prefabName = "choose_narrative";
        typeDisplay.text = passage.label;
        typeDisplay.GetComponent<TooltipHolder>().tooltip = passage.tooltip;
        typeDisplay.GetComponent<TooltipHolder>().cues = passage.cues;

        if (newCard) {
            submit.onClick.AddListener(SetFinished);
        } else {
            foreach (GameObject button in buttons) button.SetActive(false);
            textDisplay.gameObject.SetActive(true);
            textDisplay.text = passage.text;
            promptDisplay.gameObject.SetActive(true);
            promptDisplay.text = passage.text;
        }
    }

    protected override void Choose(int i) {
        promptDisplay.gameObject.SetActive(true);
        textEntry.gameObject.SetActive(true);
        submit.gameObject.SetActive(true);

        Prompt prompt = new Prompt();
        prompt.label = activeChoices[i].label;
        passage.chosenPrompt = prompt;
        if (passage.chosenPrompt == null) {
            promptDisplay.text = "";
        } else {
            promptDisplay.text = passage.chosenPrompt.label;
        }
        foreach (GameObject button in buttons) button.SetActive(false);
    }

    public bool RecordPassage(){
        if (textEntry.text == ""){
            storyManager.CallBlock("missingParagraph");
            return false;
        }
        passage.text = textEntry.text;
        textDisplay.text = textEntry.text;
        storyManager.storyHolder.story.Add(passage);
        textDisplay.gameObject.SetActive(true);
        textEntry.gameObject.SetActive(false);
        submit.gameObject.SetActive(false);
        return true;
    }


    public override void ReturnStory(){
        if (RecordPassage()){
			storyManager.FinishScene();
		}
    }
}
