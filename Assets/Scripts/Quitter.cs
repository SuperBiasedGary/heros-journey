﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class Quitter : MonoBehaviour {

    private GameManager gameManager;

    private void Awake(){
        gameManager = FindObjectOfType<GameManager>();
    }

    public void Quit(){
        DataManager.SaveSettings(gameManager.gameSettings);
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false; 
        #endif
        Application.Quit();
    }

    void SaveStory(){
        StoryManager storyManager = FindObjectOfType<StoryManager>();
        if (!storyManager) return;
        StoryHolder story = storyManager.storyHolder;
        if (story == null) return;
        DataManager.SaveStory(story);
    }
}
