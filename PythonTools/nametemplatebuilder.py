import json

from random import shuffle

NAME_PATH = "names.json"

with open(NAME_PATH) as f:
    NAME_DATA = json.load(f)

names = NAME_DATA["Pregen"]


def try_template(name, seed_value, seed_type):
    template = name.replace(seed_value, "[" + seed_type + "]")
    reduced_name = name.replace(seed_value, "|")
    changed = True
    segments = all_segments[:]
    shuffle(segments)
    while changed:
        changed = False
        for key, value in segments:
            if value in reduced_name:
                template = template.replace(value, "[" + key + "]")
                reduced_name = reduced_name.replace(value, "|")
                changed = True

    print name, ">", reduced_name, "+", template
    if not reduced_name.strip("| "):
        print "Successfully turned ", name,"into template", template
        return template


templates = set()
segments = NAME_DATA["Segments"]
del segments["t"]
all_segments = []
for k, vs in segments.items():
    for v in vs:
        all_segments.append((k, v))


for name in names:
    for key, value in all_segments:
        if value in name:
            template = try_template(name, value, key)
            if template:
                templates.add(template)
templates = sorted(templates)
print "\n".join(templates)


