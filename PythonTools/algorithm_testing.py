import random

from collections import Counter
from pprint import pprint

CONFLICT_PARAMETERS = [(x, y) for x in range(1, 7) for y in range(1, 6)]
TRAIN_PARAMETERS = [(x,) for x in range(1, 6)]
RESOLVE_PARAMETERS = [(x,) for x in range(1, 15)]

def conflict(player, opponent):
	roll1 = sum(random.randint(0, 1) for _ in range(player))
	roll2 = sum(random.randint(0, 1) for _ in range(opponent))
	if roll1 > roll2:
		if roll2 == 0 and roll1 == player and player > 1 and opponent > 1:
			return "WIN CRITICAL"
		return "WIN"
	elif roll2 > roll1:
		if roll1 == 0 and roll2 == player and player > 1 and opponent > 1:
			return "LOSS CRITICAL"
		return "LOSS"
	else:
		return "TIE"

def train(player):
    rolls = [[random.randint(0, 1) for _ in range(player + 1)]
             for _ in range(3)]
    successes = [all(roll) for roll in rolls]
    if all(successes):
        return "SUCCESS CRITICAL"
    if any(successes):
        return "SUCCESS"

    full_failures = [(not any(roll)) for roll in rolls]
    if all(full_failures):
        return "FAILURE CRITICAL"
    return "FAILURE"

def resolution(coins):
    roll = sum(random.randint(0, 1) for _ in range(coins))
    if roll > 5:
        roll = 5
    return roll


def test(function, parameter_set, run_count=10000):
	results = {}
	counter = Counter()
	for parameters in parameter_set:
		counter = Counter()
		for _ in range(run_count):
			counter[function(*parameters)] += 1
		for key in counter:
			counter[key] = (counter[key] * 100.0) / run_count
		results[parameters] = counter
	return results


def main():
    #result = test(conflict, CONFLICT_PARAMETERS)
    #pprint(result)
    #result = test(train, TRAIN_PARAMETERS, 100000)
    #pprint(result)
    result = test(resolution, RESOLVE_PARAMETERS, 100000)
    pprint(result)


if __name__ == "__main__":
    main()
