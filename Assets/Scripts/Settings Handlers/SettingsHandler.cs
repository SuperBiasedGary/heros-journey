﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingsHandler : MonoBehaviour {

    private GameSettings gameSettings;
    public Button saveSettings;
    public Button returnToMenu;
    public Toggle tutorial;
    public Toggle tooltipDefault;
    public Toggle rerollsEnabled;
    public Button tooltipKey;
    public Text tooltipKeyText;
    public TooltipHolder tooltip;
    public InputField exportInput;

    private bool capturing = false;

    private void Awake(){
        gameSettings = FindObjectOfType<GameManager>().gameSettings;
        saveSettings.onClick.AddListener(SaveSettings);
        returnToMenu.onClick.AddListener(ToMenu);

        rerollsEnabled.isOn = gameSettings.rerollsEnabled;
        tutorial.isOn = gameSettings.tutorials;
        tooltipDefault.isOn = gameSettings.tooltipDefaultOn;
        tooltipKeyText.text = gameSettings.tooltipKeycode.ToString();
        tooltipKey.onClick.AddListener(UpdateKeycode);
        exportInput.text = gameSettings.exportFolder;

        rerollsEnabled.onValueChanged.AddListener(UpdateRerollsDefault);
        tooltipDefault.onValueChanged.AddListener(UpdateTooltipDefault);
        tooltipDefault.onValueChanged.AddListener(UpdateTooltipDefault);

        //AddListeners(tutorial.gameObject);
        //AddListeners(rerollsEnabled.gameObject);
        //AddListeners(tooltipDefault.gameObject);
        //AddListeners(tooltipKey.gameObject);
    }

    void UpdateTooltipDefault(bool value){
        gameSettings.tooltipDefaultOn = value;
    }

    void UpdateRerollsDefault(bool value){
        gameSettings.rerollsEnabled = value;
    }

    public void ToMenu(){
        SceneManager.LoadScene("MainMenu");
    }


    public void SaveSettings(){
        gameSettings.rerollsEnabled = rerollsEnabled.isOn;
        gameSettings.tutorials = tutorial.isOn;
        gameSettings.tooltipDefaultOn = tooltipDefault.isOn;
        gameSettings.exportFolder = exportInput.text;
        DataManager.SaveSettings(gameSettings);
    }

    public void UpdateKeycode(){
        capturing = true;
        tooltipKeyText.text = "[Press Key]";
    }

    void Update(){
        if (!capturing) return;
        foreach (KeyCode key in System.Enum.GetValues(typeof(KeyCode))){
            if (!Input.GetKey(key)) continue;
            tooltipKeyText.text = key.ToString();
            gameSettings.tooltipKeycode = key;
            capturing = false;
        }
    }

    protected void AddListeners(GameObject gameObject){
        EventTrigger eventTrigger = gameObject.GetComponent<EventTrigger>();
        Transform parent = gameObject.transform.parent;
        while(eventTrigger == null && parent != null){
            eventTrigger = parent.GetComponent<EventTrigger>();
            parent = parent.transform.parent;
        }
        eventTrigger.AddListener(EventTriggerType.PointerEnter, SetTooltip);
        eventTrigger.AddListener(EventTriggerType.PointerExit, ClearTooltip);
    }

    void SetTooltip(PointerEventData eventData){
        tooltip = eventData.pointerCurrentRaycast.gameObject.GetComponent<TooltipHolder>();
        if (tooltip != null) tooltip.UpdateTooltip();
    }

    void ClearTooltip(PointerEventData eventData){
        if (tooltip != null) tooltip.ClearTooltip();
    }

}
