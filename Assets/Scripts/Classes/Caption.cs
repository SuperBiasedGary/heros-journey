﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Caption : Recordable {

    public string title = "";
    public string subtitle = "";

    public override string ToString(){
        string text = title;
        if (subtitle != null && subtitle.Trim() != "") text += "\n" + subtitle;
        return text;
    }


    public override string ToHtml(){
        string text = MakeHeader(title, 1);
        if (subtitle != null && subtitle.Trim() != "") text += "\n" + MakeHeader(subtitle, 4);
        return text;
    }
}
