﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PassageHandler : MonoBehaviour {

    // Change to level based structure?
    // Load a level that contains the structure of the passage baked in.

    public Text display;
    public InputField nameField;
    public InputField details;

    public string ReturnPassage(Passage passage){
        switch (passage.type){
            case "narrative":
                return "";
            case "conflict":
                return ReturnConflict(passage);
            case "post_conflict":
                return "";
            case "showdown":
                return "";
            case "epilogue":
                return "";
            default:
                throw new NotImplementedException();
        }
    }

    string ReturnNarrative(Passage currentObject){
        if (details.text == ""){
            Debug.LogWarning("Please enter a passage.");
            return "";
        }
        string text = details.text;
        details.text = "";
        return text;
    }


    string ReturnConflict(Passage passage){
        return "";
    }

}
