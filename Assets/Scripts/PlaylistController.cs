﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaylistController : MonoBehaviour {

    AudioClip[] playlist;
    AudioSource audioSource;

	void Awake () {
        audioSource = GetComponent<AudioSource>();
        playlist = LoadPlaylist("Music/wav");
        StartSong();
	}

    AudioClip[] LoadPlaylist(string path){
        return Resources.LoadAll<AudioClip>(path);
    }

    void StartSong(){
        AudioClip newSong = playlist[Random.Range(0, playlist.Length)];
        audioSource.clip = newSong;
        audioSource.Play();
        Invoke("StartSong", newSong.length);
    }
    	
}

