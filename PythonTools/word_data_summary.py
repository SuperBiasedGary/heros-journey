import re

from collections import defaultdict, Counter
from pprint import pprint
from sys import argv
from copy import deepcopy

try:
    from simplejson import load
except ImportError:
    from json import load

PATTERN = re.compile(r"\[.*?\]")

BASE = r"C:\Users\gary.chadwick\Downloads\words.json"
BASE = r"C:\Users\gchadwick\Documents\05_Other\heros-journey\Assets\StreamingAssets\words.json"
BASE = r"C:\Users\Gary\Documents\Unity Projects\heros-journey\Assets\StreamingAssets\words.json"

OBJECT_TYPES = ("Hero", "Antagonist", "Setting", "Quest", "Ally", "Obstacle")
WORD_TYPES = ("Adjective", "Noun", "Verb", "Name")

def check_file(path):
    
    data = read_file(path)
    if not data: return

    check_words(data)
    check_phrases(data)
    print path, "check complete"


def check_words(data):
    for typename in WORD_TYPES:
        print "Summarising",typename
        pprint(check_word_type(data[typename]))



def check_word_type(data):
    results = {"Total": len(data)}
    for name in OBJECT_TYPES:
        results[name] = len([value for value in data.values()
                             if name in value['tags']])
    return results


def check_phrases(data):
    print "Phrases:"
    for name in OBJECT_TYPES:
        counts = Counter()
        print name, len(data["Phrases"][name])
        for phrase in data["Phrases"][name]:
            results = PATTERN.findall(phrase)
            for result in results[:]:
                result = result.strip("[]^?+")
                if ":" in result:
                    counts.update(result.split(":"))
                else:
                    counts[result] += 1
        pprint(dict(counts))

def read_file(path):
    try:
        with open(path) as f:
            return load(f)
    except ValueError as e:
        print "Couldn't read JSON"
        print e


def main():
    if not argv[1:]:
       check_file(BASE)
    else:
        for f in argv[1:]:
            print("Analysing " + f)
            check_file(f)


if __name__ == "__main__":
    main()
    raw_input("Complete")
