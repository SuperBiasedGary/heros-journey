﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ChoiceHandler : Handler {

    public Dictionary<string, Choice> choices;
    public Choice[] activeChoices;

    public GameObject[] buttons = new GameObject[5];
    public GameObject[] choiceDisplays = new GameObject[5];
    public GameObject[] strikethroughs = new GameObject[5];

    public const int MAX_CHOICES = 5;

    public override void Awake(){
        base.Awake();

        GameObject child;
        int index;
        for (int i = 0; i < transform.childCount; i++){
            child = transform.GetChild(i).gameObject;
            int.TryParse(child.name.Substring(child.name.Length - 1), out index);
            if (child.name == "Preamble"){
                AddListeners(child);
            } else if (child.name.StartsWith("Choice")) {
                buttons[index] = child;
            } else if (child.name.StartsWith("TextChoice")) {
                choiceDisplays[index] = child;
            } else if (child.name.StartsWith("Strikethrough")) {
                strikethroughs[index] = child;
            }
        }
    }

    public override void Init(bool newCard, Passage passage){
        base.Init(newCard, passage);
        if (newCard) {
            // Handle there being less choices than the set minimum value
            passage.minimumChoices = Mathf.Min(passage.minimumChoices, passage.choices.Length);
            passage.choices = calculateChoices(false);
            for (int i = 0; i < buttons.Length; i++) SetupButton(i);
            for (int i = 0; i < choiceDisplays.Length; i++) SetupText(i);
            for (int i = activeChoices.Length; i < strikethroughs.Length; i++){
                if (strikethroughs[i] != null){
                    strikethroughs[i].GetComponent<Text>().text = "";
                }
            }
        } else {
            foreach (GameObject button in buttons) button.SetActive(false);
            Choice choice;
            for (int i = 0; i < choiceDisplays.Length; i++){
                if (choiceDisplays[i] == null) continue;

                if (i >= passage.choices.Length){
                    choiceDisplays[i].SetActive(false);
                    continue;
                }
                choice = passage.choices[i];
                choiceDisplays[i].GetComponent<Text>().text = choice.label;
                choiceDisplays[i].SetActive(true);
                if (!choice.chosenPrompt) {
                    strikethroughs[i].SetActive(true);
                }
            }
        }
    }


    [ContextMenu("TestCalculation")]
    public void TestCalculation(){
        Choice[] choices;
        List<string> strings;
        for (int _ = 0; _ < 1000; _++){
            strings = new List<string>();
            choices = calculateChoices(true);
            string result = "";
            foreach (Choice choice in choices) strings.Add(choice.label);
            strings.Sort();
            foreach (string s in strings) result += s + ";";
            Debug.Log(result);
        }
    }

    protected float CalculateWeightMax(List<Choice> choices){
        float threshold = 0f;
        float weight;
        foreach (Choice choice in choices){
            weight = ModifiedChoiceWeight(choice);
            //Debug.Log("Calculated weight " + weight.ToString() + " for " + choice.label);
            threshold += weight;
        }
        return threshold;
    }


    protected Choice[] calculateChoices(bool test){
        choices = new Dictionary<string, Choice>();
        List<Choice> potentialChoices = new List<Choice>();
        int choicesActive = 0;
        foreach (Choice choice in passage.choices){
            if (!TestValidChoice(choice)) {
                Debug.Log(choice.label + " is not valid");
                continue;
            } else if (choice.active) {
                choices[choice.label] = choice;
                choicesActive++;
            } else {
                potentialChoices.Add(choice);
            }
        }

        Choice chosen;
        int max_choices = MAX_CHOICES - choicesActive;
        for (int x = 0; x < max_choices; x++) {
            chosen = SelectChoice(potentialChoices);
            if (chosen != null){
                potentialChoices.Remove(chosen);
                choices[chosen.label] = chosen;
                choicesActive++;
            }
            if (potentialChoices.Count == 0) break;
            // Premature breaking: More likely the more choices there are already.
            if (choicesActive >= passage.minimumChoices &&
                Random.Range(0f, MAX_CHOICES) < choicesActive){
                break;
            }
        }

        activeChoices = new Choice[choicesActive];
        int i = 0;
        foreach(Choice choice in choices.Values){
            activeChoices[i] = choice;
            i++;
        }
        return activeChoices;
    }

    protected Choice SelectChoice(List<Choice> potentialChoices){
        float threshold = CalculateWeightMax(potentialChoices);
        float roll = Random.Range(0f, threshold);
        float weight;
        foreach (Choice choice in potentialChoices){
            weight = ModifiedChoiceWeight(choice);
            if (roll < weight){
                return choice;
            } else {
                roll -= weight;
            }
        }
        return null;
    }

    protected float ModifiedChoiceWeight(Choice choice){
        if (choice.modifierOperation == null || choice.modifierOperation == ""){
            return choice.weight;
        }
        float modifier = storyManager.storyHolder.GetCounter(choice.modifierName);
        switch (choice.modifierOperation){
            case "+":
                return choice.weight + modifier;
            case "-":
                float result = choice.weight - modifier;
                // Prevent negative values
                return Mathf.Max(result, 0);
            case "*":
                return choice.weight * modifier;
            case "/":
                if (modifier == 0) return choice.weight;
                return choice.weight / modifier;
            case "^":
                return (float) System.Math.Pow(choice.weight, modifier);
            default:
                if (choice.modifierOperation != "") Debug.Log("Invalid operation: " + choice.modifierOperation);
                return choice.weight;
        }
    }


    protected bool TestValidChoice(Choice choice){
        switch (choice.function){
            case "Retirement":
                return RetirementCheck(choice);
            case "PowerLoss":
                return PowerLossCheck(choice);
            case "Exists":
                return ExistenceCheck(choice);
            case "FlagIs":
                return BlockFlagCheck(choice);
            default:
                if (choice.function != null && choice.function != "") Debug.Log("Invalid function name " + choice.function);
                return true;
        }
    }


    protected bool BlockFlagCheck(Choice choice){
        string result = storyManager.storyHolder.blockFlag;
        switch (choice.selector){
            case "IS":
                return result == choice.requirement;
            case "IS_NOT":
                return result != choice.requirement;
            default:
                Debug.Log("Invalid selector " + choice.selector);
                return true;
        }
    }


    protected bool ExistenceCheck(Choice choice){
        List<StoryObject> storyObjects;
        string result;
        switch (choice.selector){
            case "ANY":
                storyObjects = storyManager.storyHolder.GetStoryObjects(choice.applicableTypes);
                result = (storyObjects.Count > 0).ToString();
                return result == choice.requirement;
            case "ALL":
                foreach (string typename in choice.applicableTypes){
                    storyObjects = storyManager.storyHolder.GetStoryObjects(typename);
                    result = (storyObjects.Count > 0).ToString();
                    if (result != choice.requirement) return false;
                }
                // None were missing
                return true;
            default:
                Debug.Log("Invalid selector " + choice.selector);
                return true;
        }
    }

    protected bool RetirementCheck(Choice choice){
        List<StoryObject> storyObjects = storyManager.storyHolder.GetStoryObjects(choice.applicableTypes);
        switch (choice.selector){
            case "ANY":
                foreach (StoryObject storyObject in storyObjects){
                    if (storyObject.retired.ToString() == choice.requirement) return true;
                }
                // None matched
                return false;
            case "ALL":
                if (storyObjects.Count == 0) return false;
                foreach (StoryObject storyObject in storyObjects){
                    if (storyObject.retired.ToString() != choice.requirement) return false;
                }
                // All matched
                return true;
            case "FIRST":
                if (storyObjects.Count == 0) return false;
                return storyObjects[0].retired.ToString() == choice.requirement;
            case "LAST":
                if (storyObjects.Count == 0) return false;
                return storyObjects[-1].retired.ToString() == choice.requirement;
            case "RECENT":
                StoryObject recent = storyManager.storyHolder.GetRecent(choice.applicableTypes[0]);
                if (recent == null) return "false" == choice.requirement;
                return recent.retired.ToString() == choice.requirement;
            case "EXIST":
                return storyObjects.Count > 0;
            case "NONEXIST":
                return storyObjects.Count == 0;
            default:
                Debug.Log("Invalid selector for Retirement Check: " + choice.selector);
                return true;
        }
    }


    protected bool PowerLossCheck(Choice choice){
        List<StoryObject> storyObjects = storyManager.storyHolder.GetStoryObjects(choice.applicableTypes);

        switch (choice.selector){
            case "ANY":
                foreach (StoryObject storyObject in storyObjects){
                    if (storyObject.HasLostPower().ToString() == choice.requirement) return true;
                }
                // None matched
                return false;
            case "ALL":
                if (storyObjects.Count == 0) return false;
                foreach (StoryObject storyObject in storyObjects){
                    if (storyObject.HasLostPower().ToString() != choice.requirement) return false;
                }
                // All matched
                return true;
            case "FIRST":
                if (storyObjects.Count == 0) return false;
                return storyObjects[0].HasLostPower().ToString() == choice.requirement;
            case "LAST":
                if (storyObjects.Count == 0) return false;
                return storyObjects[-1].HasLostPower().ToString() == choice.requirement;
            case "EXIST":
                return storyObjects.Count > 0;
            case "NONEXIST":
                return storyObjects.Count == 0;
            default:
                Debug.Log("Invalid selector " + choice.selector);
                return true;
        }
    }


    protected void SetupText(int i){
        if (choiceDisplays[i] == null) return;
        if (i > activeChoices.Length - 1) choiceDisplays[i].GetComponent<Text>().text = "";
        else choiceDisplays[i].GetComponent<Text>().text = activeChoices[i].label;
    }

    protected void SetupButton(int i){
        GameObject button = buttons[i];
        if (i >= activeChoices.Length){
            button.SetActive(false);
            return;
        }
        Choice choice = activeChoices[i];
        button.name = choice.label;
        GameObject child = button.transform.GetChild(0).gameObject;
        child.GetComponent<Text>().text = choice.label;
        if (choice.tooltip != ""){
            AddListeners(child);
            child.GetComponent<TooltipHolder>().tooltip = choice.tooltip;
            child.GetComponent<TooltipHolder>().cues = choice.cues;
        }
        button.GetComponent<Button>().onClick.AddListener(() => Choose(i));
    }

    protected virtual void Choose(int i){
        passage.chosenChoice = activeChoices[i];
        passage.chosenChoice.chosenPrompt = true;
        passage.prefabName = "choose";
        storyManager.storyHolder.story.Add(passage);
        storyManager.storyHolder.blockFlag = passage.chosenChoice.flag;

        foreach (GameObject button in buttons) button.SetActive(false);
        foreach (GameObject display in choiceDisplays) display.SetActive(true);
        for (int j = 0; j < strikethroughs.Length; j++){
            if (i != j) strikethroughs[j].SetActive(true);
        }
        ReturnStory();
    }

    public override void ReturnStory(){
        storyManager.FinishScene();
    }
}
