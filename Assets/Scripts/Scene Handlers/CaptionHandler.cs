﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CaptionHandler : Handler{

    private Caption caption;

    private Text title;
    private Text subTitle;
    private Button button;

    public override void Awake() {
        base.Awake();
        caption = new Caption();
        caption.prefabName = "caption";
        int children = transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Caption":
                    title = child.GetComponent<Text>();
                    break;
                case "Sub Caption":
                    subTitle = child.GetComponent<Text>();
                    break;
                case "Continue":
                    button = child.GetComponent<Button>();
                    break;
            }
        }
    }

    public override void Init(bool newCard, Passage passage){
        base.Init(newCard, passage);

        if (newCard){
            button.onClick.AddListener(ReturnStory);
            AddListeners(button.transform.GetChild(0).gameObject);
        } else {
            button.gameObject.SetActive(false);
        }

        title.text = passage.label;
        caption.title = passage.label;

        if (passage.tooltip != ""){
            subTitle.text = passage.tooltip;
            caption.subtitle = passage.tooltip;
        } else {
            subTitle.gameObject.SetActive(false);
        }
    }

    public override void ReturnStory(){
        button.gameObject.SetActive(false);
        storyManager.storyHolder.story.Add(caption);
        storyManager.FinishScene();
    }
}
