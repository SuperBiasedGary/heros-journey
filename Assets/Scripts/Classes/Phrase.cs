﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class Phrase {

    private List<string> words = new List<string>();
    JSONNode wordData;
    JSONNode currentWord;
    string parent_type;
    string VOWELS = "aeiouAEIOU";

    private string FindNextWord(string source){
        int end;
        if (source.StartsWith("[")){
            end = source.IndexOf(']') + 1;
        } else {
            end = source.IndexOf('[');
        }
        if (end < 0){
            return source;
        } else {
            return source.Substring(0, end);
        }
    }

    public void Init(string template){
        if (template == null){
            Debug.LogWarning("Null template passed");
            return;
        }

        words = new List<string>();
        while (template != ""){
            string word = FindNextWord(template);
            words.Add(word);
            template = template.Remove(0, word.Length);
        }
    }

    public string Roll(JSONNode wordData, string typename){
        string template = "";
        foreach (JSONNode template_set in wordData["Phrases"][typename].Values){
            template += template_set[Random.Range(0, template_set.Count)];
        }
        template = template.TrimStart('\n');
        string result = Roll(wordData, typename, template);
        return result;
    }

    public string Roll(JSONNode wordData, string typename, string template) {
        parent_type = typename;
        this.wordData = wordData;
        Init(template);

        string word;
        for(int i = 0; i < words.Count; i++){
            word = words[i];
            if (!word.StartsWith("[") || !word.EndsWith("]")) continue;
            words[i] = Parse(word);
        }
        return ToString();
    }

    private string Parse(string word){
        word = word.Trim(new char[] {'[', ']'});
        int title = 0;
        if (word[0] == '^'){
            if (word[1] == '?'){
                title = 1;
                word = word.Substring(2);
            } else {
                title = 2;
                word = word.Substring(1);
            }
        }

        string tense;
        switch (word[0]){
            case '-':
                word = word.Substring(1);
                tense = "Past";
                break;
            case '+':
                word = word.Substring(1);
                tense = "Present";
                break;
            case '*':
                word = word.Substring(1);
                tense = "Participle";
                break;
            default:
                tense = "";
                break;
        }

        int plural = 0;
        string end = word.Substring(word.Length - 2);
        if (end == "+?"){
            plural = 1;
            word = word.Substring(0, word.Length - 2);
        } else if (end[1] == '+'){
            plural = 2;
            word = word.TrimEnd('+');
        }

        string tag;
        if (word.Contains(":")){
            string[] split = word.Split(':');
            word = split[0];
            tag = split[1];
        } else {
            tag = parent_type;
        }

        string result = Pick(word, tag);
        if (result == "") return "";
        result = Style(result, title, plural, tense);
        return result;
    }

    private string PhrasePick(string phrase_type){
        Phrase phrase = new Phrase();
        if (phrase_type == "Title") {
            phrase.Roll(wordData, phrase_type);
        } else {
            phrase.Roll(wordData, phrase_type, "[Adjective] [Noun]");
        }
        return phrase.ToString();
    }

    private string Pick(string wordtype, string tag) {
        currentWord = null;
        List<string> valid_words = new List<string>();
        JSONNode node;

        foreach (string phrase_type in wordData["Phrases"].Keys){
            if (phrase_type == wordtype) return PhrasePick(wordtype);
        }

        foreach(string word in wordData[wordtype].Keys){
            node = wordData[wordtype][word];
            foreach (string tag_name in node["tags"].Values){
                if (tag_name == tag){
                    valid_words.Add(word);
                    break;
                }
            }
        }
        if (valid_words.Count == 0) {
            Debug.LogWarning("No valid words found for " + wordtype);
            return "";
        }
        string choice = valid_words[Random.Range(0, valid_words.Count)];
        currentWord = wordData[wordtype][choice];
        return choice;
    }

    private string Style(string word, int title, int plural, string tense){
        if (currentWord != null && Random.Range(0f, 2f) <= plural){
            string plural_word = currentWord["plural"].Value;
            if (plural_word != "") word = plural_word;
        }
        if (currentWord != null && tense != ""){
            string conjugated = currentWord[tense].Value;
            if (conjugated != "") word = conjugated;
        }

        if (Random.Range(0f, 2f) <= title){
            word = Capitalise(word);
        }
        return word;
    }

    private string Capitalise(string s){
        if (s == "" || s == null) return "";
        if (s.Length == 1) return s.ToUpper();
        return s[0].ToString().ToUpper() + s.Substring(1);
    }

    public override string ToString() {
        string result = "";
        foreach (object word in words){
            result += word.ToString();
        }
        string target;
        string source;
        foreach (char vowel in VOWELS){
            source = " a " + vowel;
            target = " an " + vowel;
            result = result.Replace(source, target);
        }
        return Capitalise(result);
	}
}
