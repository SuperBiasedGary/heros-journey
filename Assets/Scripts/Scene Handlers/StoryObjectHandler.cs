using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StoryObjectHandler : Handler {

    public StoryObject me = null;
    private const int PROMPT_OPTIONS = 5;

    Text type;
    Text powerText;
    Text title;
    Text nameDisplay;
    Text descriptionDisplay;

    GameObject[] buttons = new GameObject[5];
    string[] promptOptions = new string[5];
    public Button submit;
    Button reroll;
    InputField nameInput;
    InputField descriptionInput;

    bool powered = false;
    bool focused = true;
    TooltipHolder currentTooltip;

    public override void Awake(){
        base.Awake();
        storyManager = FindObjectOfType<StoryManager>();

        int children = transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;

            int x;
            string prefix;
            if (child.name.StartsWith("PhraseButton")){
                prefix = child.name[child.name.Length - 1].ToString();
                x = Int32.Parse(prefix);
                buttons[x] = child;
                continue;
            }

            switch (child.name){
                case "Type":
                    type = child.GetComponent<Text>();
                    AddListeners(child);
                    break;
                case "Power":
                    AddListeners(child);
                    powerText = child.GetComponent<Text>();
                    break;
                case "NameInput":
                    nameInput = child.GetComponent<InputField>();
                    AddListeners(child);
                    break;
                case "Name":
                    nameDisplay = child.GetComponent<Text>();
                    break;
                case "DescriptionInput":
                    descriptionInput = child.GetComponent<InputField>();
                    AddListeners(child);
                    break;
                case "Description":
                    descriptionDisplay = child.GetComponent<Text>();
                    descriptionDisplay.GetComponent<Text>().text = me.details;
                    break;
                case "Reroll":
                    reroll = child.GetComponent<Button>();
                    reroll.onClick.AddListener(Reroll);
                    break;
                case "Submit":
                    submit = child.GetComponent<Button>();
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }

    public override void ReturnStory(){
        storyManager.storyHolder.Store(me);
        me.prefabName = "definition";
        storyManager.storyHolder.story.Add(me);
        storyManager.FinishScene();
    }

    public void LoadOldCard(){
        //Deactivating inputs
        nameInput.gameObject.SetActive(false);
        descriptionInput.gameObject.SetActive(false);
        reroll.gameObject.SetActive(false);
        submit.gameObject.SetActive(false);

        foreach (GameObject button in buttons){
            button.SetActive(false);
        }

        //Enabling displays
        nameDisplay.text = me.GetName();
        nameDisplay.gameObject.SetActive(true);
        descriptionDisplay.text = me.details;
        descriptionDisplay.gameObject.SetActive(true);
    }

    void Reroll(){
        me.Roll();
        UpdatePhraseButtons();
    }

    public override void Update(){
        base.Update();
        if (focused && Input.GetButton("Copy") &&
            (Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl))){
            GUIUtility.systemCopyBuffer = me.ToString();
        }

        if (!powered) return;
        if (me.retired)  powerText.text = "RETIRED";
        else powerText.text = "Power " + me.GetPower();
    }

    public override void Init(bool newCard, Passage passage){
        base.Init(newCard, passage);

        if (storyManager.storyHolder.currentObject != null) {
            me = storyManager.storyHolder.currentObject;
        } else { 
            me = storyManager.gameManager.templates.templateObjects[passage.objectType].Copy();
            me.Roll();
            storyManager.storyHolder.currentObject = me;
        }
        DataManager.SaveStory(storyManager.storyHolder);

        if (newCard){
            nameDisplay.gameObject.SetActive(false);
            descriptionDisplay.gameObject.SetActive(false);
        }
        powered = me.GetPower() > 0;
        if (!powered) powerText.gameObject.SetActive(false);

        type.text = me.label;

        if (newCard) UpdatePhraseButtons();
        submit.onClick.AddListener(Define);
        if (!me.named) nameInput.gameObject.SetActive(false);
        type.GetComponent<TooltipHolder>().tooltip = me.tooltip;
        type.GetComponent<TooltipHolder>().cues = me.cues;
    }


    public void Init(bool newCard, StoryObject storyObject){
        me = storyObject;
        powered = me.GetPower() > 0;
        if (!powered) powerText.gameObject.SetActive(false);

        LoadOldCard();
        type.text = me.label;
        if (!me.named) nameInput.gameObject.SetActive(false);
        type.GetComponent<TooltipHolder>().tooltip = me.tooltip;
        type.GetComponent<TooltipHolder>().cues = me.cues;
    }

    public void LoadObject(StoryObject storyObject){
        me = storyObject;
        powered = me.GetPower() > 0;
        if (!powered) powerText.gameObject.SetActive(false);

        type.text = me.label;
        if (!me.named) nameDisplay.gameObject.SetActive(false);
        else nameDisplay.text = me.GetName();

        descriptionDisplay.text = me.details;
        type.GetComponent<TooltipHolder>().tooltip = me.tooltip;
        type.GetComponent<TooltipHolder>().cues = me.cues;
    }

    void UpdatePhraseButtons(){
        Phrase phrase = new Phrase();
        string stringPhrase;
        for (int i = 0; i < buttons.Length; i++){
            phrase.Roll(storyManager.gameManager.templates.wordData, passage.objectType);
            stringPhrase = phrase.ToString();
            promptOptions[i] = stringPhrase;
            buttons[i].GetComponentInChildren<Text>().text = stringPhrase;
            buttons[i].GetComponent<Button>().onClick.AddListener(ChooseWrapper(i));
        }
    }

    UnityAction ChooseWrapper(int i){
        return () => { Choose(i); };
    }

    void Choose(int i){
        foreach (GameObject button in buttons){
            button.SetActive(false);
        }
        descriptionInput.gameObject.SetActive(true);
        descriptionInput.text = promptOptions[i];
        reroll.gameObject.SetActive(false);
    }

    public void Define(){
        if (descriptionInput.text == "") storyManager.CallBlock("missingDescription");
        if (me.named && nameInput.text == "") storyManager.CallBlock("missingName");
        if (descriptionInput.text == "" || (me.named && nameInput.text == "")) return;

        if (me.named){
            nameDisplay.gameObject.SetActive(true);
            nameDisplay.GetComponent<Text>().text = nameInput.text;
            me.SetName(nameInput.text);
            nameInput.gameObject.SetActive(false);
        }
        descriptionDisplay.gameObject.SetActive(true);
        descriptionDisplay.GetComponent<Text>().text = descriptionInput.text;
        me.details = descriptionInput.text;
        descriptionInput.gameObject.SetActive(false);

        submit.onClick.RemoveAllListeners();
        submit.gameObject.SetActive(false);
        reroll.gameObject.SetActive(false);
        ReturnStory();
    }
}
