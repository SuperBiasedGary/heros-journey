﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StoryHolder {

    private Dictionary<string, List<StoryObject>> storyObjects = new Dictionary<string, List<StoryObject>>();
    private Dictionary<string, StoryObject> recentObject = new Dictionary<string, StoryObject>();

    public List<Recordable> story = new List<Recordable>();
    public List<string> finalResults = new List<string>();
    public List<string> flags = new List<string>();

    public int passageNumber = 0;
    public Passage currentPassage;
    public StoryObject currentObject;
    public string blockFlag;
    public string blockKey;
    public List<Modifier> modifiers = new List<Modifier>();
    private Dictionary<string, int> counters = new Dictionary<string, int>();

    public void SetCounter(string counter, int number){
        if (counters.ContainsKey(counter)) Debug.LogWarning(counter + " already exists in dictionary");
        counters[counter] = number;
    }

    public void InitCounter(string counter){
        if (counters.ContainsKey(counter)) Debug.LogWarning(counter + " already exists in dictionary");
        counters[counter] = 0;
    }

    public int GetCounter(string counter){
        if (!counters.ContainsKey(counter)){
            Debug.LogWarning(counter + " does not exist in dictionary");
            return 0;
        }
        return counters[counter];
    }

    public void AddToCounter(string counter, int value){
        if (!counters.ContainsKey(counter)) Debug.LogWarning(counter + " does not exist in dictionary");
        else counters[counter] += value;
    }


    public bool IsAvailable(string type){
        List<StoryObject> objects = GetAll(type);
        if (objects == null) return false;
        foreach (StoryObject storyObject in objects){
            if (storyObject != null && !storyObject.retired) return true;
        }
        return false;
    }

    public StoryObject GetStoryObject(string type, string flag){
        switch (flag){
            case "FIRST":
                return GetFirst(type);
            case "LAST":
                return GetLast(type);
            case "RANDOM":
                return GetRandom(type);
            case "RECENT":
                return GetRecent(type);
            default:
                Debug.LogWarning("No valid function for " + flag);
                return null;
        }
    }

    public List<StoryObject> GetStoryObjects(string type){
        return GetStoryObjects(type, "ALL");
    }

    public List<StoryObject> GetStoryObjects(string[] typenames){
        return GetStoryObjects(typenames, "ALL");
    }

    public List<StoryObject> GetStoryObjects(string[] typenames, string flag){
        List<StoryObject> storyObjects = new List<StoryObject>();
        foreach (string typename in typenames){
            storyObjects.AddRange(GetStoryObjects(typename, flag));
        }
        return storyObjects;
    }

    public List<StoryObject> GetStoryObjects(string type, string flag){
        switch (flag){
            case "ACTIVE":
                return GetAllActive(type);
            case "RETIRED":
                return GetAllRetired(type);
            case "ALL":
                return GetAll(type);
            default:
                Debug.LogWarning("No valid function for " + flag);
                return null;
        }
    }

    public void ConvertStoryObject(StoryObject storyObject, string newType){
        Remove(storyObject);
        storyObject.label = newType;
        Store(storyObject);
    }

    public List<StoryObject> GetAll(string type){
        if (type == "All"){
            List<StoryObject> objects = new List<StoryObject>();
            foreach (string key in storyObjects.Keys){
                objects.AddRange(storyObjects[key]);
            }
            return objects;
        }
        if (type == null || !storyObjects.ContainsKey(type)) return new List<StoryObject>();
        return storyObjects[type];
    }

    private List<StoryObject> GetAllActive(string type){
        if (type == null || !storyObjects.ContainsKey(type)) return null;
        List<StoryObject> results = new List<StoryObject>();
        foreach(StoryObject storyObject in storyObjects[type]){
            if (!storyObject.retired) results.Add(storyObject);
        }
        return results;
    }

    private List<StoryObject> GetAllRetired(string type){
        if (type == null || !storyObjects.ContainsKey(type)) return null;
        List<StoryObject> results = new List<StoryObject>();
        foreach (StoryObject storyObject in storyObjects[type]){
            if (storyObject.retired) results.Add(storyObject);
        }
        return results;
    }

    public void ClearRecent(string typename){
        recentObject[typename] = null;
    }

    public void ClearRecent(string[] typenames){
        foreach(string typename in typenames){
            ClearRecent(typename);
        }
    }

    public void SetRecent(StoryObject storyObject){
        if (storyObject == null) return;
        recentObject[storyObject.label] = storyObject;
    }

    public StoryObject GetRecent(string type){
        if (type == null || !recentObject.ContainsKey(type)) return null;
        return recentObject[type];
    }

    private StoryObject GetFirst(string type){
        if (type == null) return null;
        List<StoryObject> objects = GetAll(type);
        if (objects == null || objects.Count == 0){
            Debug.LogWarning("No Objects of type " + type + " to get.");
            return null;
        }
        SetRecent(objects[0]);
        return objects[0];
    }

    private StoryObject GetLast(string type){
        if (type == null) return null;
        List<StoryObject> objects = GetAll(type);
        if (objects == null || objects.Count == 0){
            Debug.LogWarning("No Objects of type " + type + "to get.");
            return null;
        }
        SetRecent(objects[objects.Count - 1]);
        return objects[objects.Count - 1];
    }

    public StoryObject GetRandom(string[] types){
        string chosenType = types[UnityEngine.Random.Range(0, types.Length - 1)];
        return GetRandom(chosenType);
    }

    public StoryObject GetRandom(string type){
        if (type == null) return null;
        List<StoryObject> objects = GetAll(type);
        if (objects == null || objects.Count == 0){
            Debug.LogWarning("No Objects of type " + type + "to get.");
            return null;
        }
        int count = objects.Count - 1;
        StoryObject storyObject = objects[UnityEngine.Random.Range(0, count)];
        SetRecent(storyObject);
        return storyObject;
    }

    public void Store(StoryObject storyObject){
        currentObject = null;
        SetRecent(storyObject);
        if (!storyObjects.ContainsKey(storyObject.label) || storyObjects[storyObject.label] == null){
            storyObjects[storyObject.label] = new List<StoryObject>();
        }
        storyObjects[storyObject.label].Add(storyObject);
    }

    public void Remove(StoryObject storyObject){
        if (!storyObjects.ContainsKey(storyObject.label) || storyObjects[storyObject.label] == null){
            return;
        }
        storyObjects[storyObject.label].Remove(storyObject);
    }
}
