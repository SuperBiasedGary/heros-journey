﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Report : Recordable {

    public string header;
    public List<Modifier> modifiers = new List<Modifier>();
    public Dictionary<string, string[]> flips = new Dictionary<string, string[]>();
    public string result;
    public StoryObject chosenStoryObject;
    public string applicableTypestring;

    public override string ToString(){
        string text = header;

        if (modifiers.Count > 0){
            text += "\nCoin Pool Modifiers:";
            foreach (Modifier modifier in modifiers){
                text += "\n" + modifier.ToString();
            }
            text += "\n";
        }

        foreach (string key in flips.Keys){
            text += "\n" + key + " Flips";
            foreach (string flip in flips[key]) text += "\n" + flip;
            text += "\n";
        }
        return text + result;
    }

    public int LongestFlip(){
        int result = 0;
        foreach(KeyValuePair<string, string[]> pair in flips){
            result = Mathf.Max(result, pair.Value.Length);
        }
        return result;
    }

    public string[] FlipKeys(){
        string[] strings = new string[flips.Count];
        int added = 0;
        foreach (KeyValuePair<string, string[]> pair in flips){
            strings[added] = pair.Key;
            added++;
        }
        return strings;
    }

    public string[] GetFlips(int index){
        string[] strings = new string[flips.Count];
        int added = 0;
        foreach (KeyValuePair<string, string[]> pair in flips){
            if (index < pair.Value.Length) strings[added] = pair.Value[index];
            else strings[added] = "";
            added++;
        }
        return strings;
    }

    public override string ToHtml(){
        string text = MakeHeader(header, 2);

        if (modifiers.Count > 0){
            text += "\n" + MakeHeader("Coin Pool Modifiers:", 3);
            string[] mods = new string[modifiers.Count];
            for (int i = 0; i < mods.Length; i++) mods[i] = modifiers[i].ToString();
            text += "\n" + MakeList(mods, false);
        }

        foreach (string key in flips.Keys){
            text += "\n" + MakeHeader(key + " Flips", 3);
            text += "\n" + MakeList(flips[key], false);
        }
        return text + "\n" + MakeHeader(result, 3);
    }
}
