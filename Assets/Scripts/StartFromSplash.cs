﻿#if (UNITY_EDITOR) 
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class StartFromSplash : EditorWindow
{
    void OnGUI(){
        // Or set the start Scene from code
        var scenePath = "Assets/Scenes/Splash.unity";
        if (GUILayout.Button("Set start scene: " + scenePath))
            SetPlayModeStartScene(scenePath);
    }

    void SetPlayModeStartScene(string scenePath)
    {
        SceneAsset myWantedStartScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath);
        if (myWantedStartScene != null)
            EditorSceneManager.playModeStartScene = myWantedStartScene;
        else
            Debug.Log("Could not find scene " + scenePath);
    }

    [MenuItem("Test/Open")]
    static void Open()
    {
        GetWindow<StartFromSplash>();
    }
}
#endif