﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectObjectHandler : Handler {

    private Report report;
    private Text preamble;
    private Button confirmButton;
    private StoryObject chosen;
    private List<Button> cardButtons;

    public override void Awake(){
        base.Awake();
        int children = transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Preamble":
                    preamble = child.GetComponent<Text>();
                    AddListeners(child);
                    break;
                case "Confirm":
                    confirmButton = child.GetComponent<Button>();
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }

    public override void Init(bool newCard, Passage passage){
        Init(newCard, passage, null);
    }


    public override void Init(bool newCard, Passage passage, Recordable recordable){
        base.Init(newCard, passage);
        if (newCard) {
            report = new Report();
            
            if (passage.objectTypeFunction == "RANDOM"){
                confirmButton.gameObject.SetActive(false);
                chosen = storyManager.storyHolder.GetRandom(passage.objectType);
                report.chosenStoryObject = chosen;
                CalculatePreamble();
                ReturnStory();
            } else {
                confirmButton.onClick.AddListener(ReturnStory);
                cardButtons = storyManager.InitialiseButtons(new string[] {passage.objectType}, "RETIRE_CHECK");
                StoryManager.selectDelegate += ChooseObject;
                if (cardButtons.Count == 1) cardButtons[0].onClick.Invoke();
            }
        } else {
            report = recordable as Report;
            chosen = report.chosenStoryObject;
            confirmButton.gameObject.SetActive(false);
        }
        CalculatePreamble();
    }

    private void CalculatePreamble(){
        if (chosen != null) {
            preamble.text = "\nSelected " + chosen.GetFullName();
        } else {
            preamble.text = "Choose any " + passage.objectType;
        }
    }

    void ChooseObject(){
        if (storyManager.selectedObject == null) return;
        StoryObject choice = storyManager.selectedObject;

        if (choice.label != passage.objectType){
            storyManager.CallBlock("wrongTypeTraining");
            return;
        }

        chosen = choice;
        report.chosenStoryObject = chosen;
        CalculatePreamble();
    }

    public override void ReturnStory(){
        report.prefabName = "select_object";
        storyManager.storyHolder.SetRecent(chosen);
        storyManager.storyHolder.story.Add(report);
        storyManager.FinishScene();
    }
}
