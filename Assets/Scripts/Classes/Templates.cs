﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Templates {
    public Dictionary<string, StoryObject> templateObjects;
    public Dictionary<string, Passage> templatePassages;
    public JSONNode storyStructure;
    public JSONNode wordData;
    public JSONNode nameData;
}
