﻿using System;
using System.IO;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using Fungus;
using System.Collections.Generic;

public class StoryManager : MonoBehaviour {

    public GameObject storyObjectCardPrefab;
    public GameObject storyObjectChoicePrefab;
    public GameObject canvasPrefab;
    public GameObject textScrapPrefab;
    public GameObject doubleTextScrapPrefab;
    public GameObject tripleTextScrapPrefab;
    public GameManager gameManager;
    private int currentBookmark = 0;
    public List<double> bookmarks = new List<double>();

    public GameObject objectCard;
    public List<Button> createdButtons;

    public JSONParser jsonParser;
    public StoryHolder storyHolder;

    [HideInInspector] public Transform canvas;
    private Transform canvasContent;
    private Transform rightContent;
    private ScrollRect scrollRect;
    private float canvasY = 0;
    private float rightCanvasY = 0;

    public StoryObject selectedObject;
    public StoryObjectHandler focusedCard;
    private JSONNode storyStructure; // Reload from gameManager
    public JSONNode currentBlock;
    private const float SCROLL_SPEED_BASE = 0.05f;
    private const float SCROLL_WAIT_INTERVAL = 0.01f;

    public delegate void StoryObjectSelectDelegate();
    public static event StoryObjectSelectDelegate selectDelegate;

    private void Awake(){
        DontDestroyOnLoad(this.gameObject);
        gameManager = FindObjectOfType<GameManager>();
        jsonParser = gameManager.GetComponent<JSONParser>();
        storyHolder = new StoryHolder();
    }

    public void Init(){
        Debug.Log("Initiating StoryManager");
        if (canvas == null){
            canvas = Instantiate(canvasPrefab).transform;
            DontDestroyOnLoad(canvas);
        }

        storyStructure = gameManager.templates.storyStructure;
        GameObject child;
        for (int i = 0; i < canvas.childCount; i++){
            child = canvas.GetChild(i).gameObject;
            switch (child.name){
                case "Scroll View":
                    scrollRect = child.GetComponent<ScrollRect>();
                    canvasContent = GetContent(child);
                    break;
                case "RightScroll":
                    rightContent = GetContent(child);
                    break;
            }
        }
    }

    Transform GetContent(GameObject parent){
        GameObject child;
        Transform transform = parent.transform;
        for (int i = 0; i < transform.childCount; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Viewport":
                    return child.transform.GetChild(0);
            }
        }
        return null;
    }

    public void CallBlock(string blockName){
        Block block = gameManager.flowchart.FindBlock(blockName);
        if (block) gameManager.flowchart.ExecuteBlock(blockName);
    }

    public void StartStory(){
        InitBlock("START");
    }

    private void Update(){
        if (Input.GetKeyDown(KeyCode.Tab)){
            currentBookmark += 1;

            if (currentBookmark >= bookmarks.Count){
                ScrollToBottom();
                currentBookmark = -1;
            }
            else ScrollToBookmark();
        }
    }

    public void Load(){
        gameManager.flowchart.SetBooleanVariable("tutorials", gameManager.gameSettings.tutorials);
        foreach (Recordable recordable in storyHolder.story) LoadRecordable(recordable);

        if (storyHolder.blockKey == "END"){
            InitPassage("Save");
            currentBlock = null;
            return;
        }
        currentBlock = storyStructure[storyHolder.blockKey];
        InitPassage(currentBlock["passages"][storyHolder.passageNumber]);
        return;
    }

    public void InitBlock(string key){
        storyHolder.passageNumber = 0;
        storyHolder.blockKey = key;
        Debug.Log("Initing Block " + key.ToString());
        if (key == "END"){
            InitPassage("Save");
            currentBlock = null;
            return;
        }
        currentBlock = storyStructure[key];
        if (currentBlock == null){
            Debug.LogError("No '" + key + "' block found.");
        } else if (currentBlock["passages"] == null || currentBlock["passages"].Count == 0){
            Debug.LogError("No passages found in '" + key + "' block.");
        }
        Debug.Log("initing passage");
        InitPassage(currentBlock["passages"][0]);
    }

    void InitPassage(string type){
        Debug.Log("Initing passage type " + type);
        SetupStoryObjectButtons();
        Passage passage;
        if (storyHolder.currentPassage != null){
            passage = storyHolder.currentPassage;
        } else {
            passage = gameManager.templates.templatePassages[type].Copy();
            storyHolder.currentPassage = passage;
            passage.Roll();
        }
        GameObject result = AddUIPrefab(passage.type);
        switch (passage.type) {
            case "raise_power":
            case "restore_power":
            case "convert_object":
            case "quest_modifier":
            case "resolution":
            case "conflict":
                result.GetComponent<Handler>().Init(true, passage, null);
                break;
            default:
                result.GetComponent<Handler>().Init(true, passage);
                break;
        }
    }

    void LoadRecordable(Recordable recordable){
        switch (recordable.prefabName){
            case "raise_power":
            case "restore_power":
            case "convert_object":
            case "quest_modifier":
            case "resolution":
            case "conflict":
            case "epilogue":
            case "change_field":
                LoadRecordablePassage(recordable);
                break;
            case "definition":
                StoryObject storyObject = recordable as StoryObject;
                LoadDefinition(storyObject);
                break;
            case "narrative":
            case "choose_narrative":
            case "prewritten":
            case "choose":
            case "yesno":
                Passage passage = recordable as Passage;
                LoadPassage(passage);
                break;
            case "caption":
                Caption caption = recordable as Caption;
                LoadCaption(caption);
                break;
            default:
                Debug.Log("Couldn't Load unsupported prefab type " + recordable.prefabName);
                break;
        }
    }

    void LoadDefinition(StoryObject storyObject){
        GameObject result = AddUIPrefab("definition");
        result.GetComponent<StoryObjectHandler>().Init(false, storyObject);
    }

    void LoadCaption(Caption caption){
        Passage passage = new Passage();
        passage.label = caption.title;
        passage.tooltip = caption.subtitle;
        passage.type = caption.prefabName;
        LoadPassage(passage);
    }

    void LoadPassage(Passage passage){
        GameObject result = AddUIPrefab(passage.type);
        result.GetComponent<Handler>().Init(false, passage);
    }

    void LoadRecordablePassage(Recordable recordable){
        GameObject result = AddUIPrefab(recordable.prefabName);
        result.GetComponent<Handler>().Init(false, null, recordable);
    }

    public void FinishScene(){
        storyHolder.passageNumber++;
        storyHolder.currentPassage = null;
        NextPassage();
    }

    public void RepeatScene(){
        storyHolder.currentPassage = null;
        NextPassage();
    }

    void NextPassage(){
        if (currentBlock == null) return;
        else if (storyHolder.passageNumber >= currentBlock["passages"].Count) {
            if (currentBlock["goto"].IsString) InitBlock(currentBlock["goto"]);
            else InitBlock(currentBlock["goto"][storyHolder.blockFlag]);
        } else {
            InitPassage(currentBlock["passages"][storyHolder.passageNumber]);
        }
    }

    public void AddFlag(string flag){
        storyHolder.flags.Add(flag);
    }

    public string StringifyFlags(){
        string result = "";
        foreach (string flag in storyHolder.flags) result += flag + "\n";
        storyHolder.flags.Clear();
        return result.Trim();
    }

    public void DestroyCardButtons(){
        foreach (Button button in createdButtons) Destroy(button.transform.parent.gameObject);
        createdButtons = new List<Button>();
        rightCanvasY = 0;
    }


    public List<Button> InitialiseButtons(string[] types, string flag){
        StoryObject storyObject;
        bool valid;
        List<Button> initialisedButtons = new List<Button>();
        foreach (Button button in createdButtons){
            button.interactable = false;
            StoryObjectHolder holder = button.GetComponent<StoryObjectHolder>();
            if (holder == null){
                Debug.Log("Null holder");
                continue;
            }
            storyObject = holder.storyObject;
            valid = false;
            foreach(string typename in types){
                if (typename == storyObject.label){
                    valid = true;
                    break;
                }
            }
            if (!valid) continue;

            switch (flag){
                case "EPILOGUE":
                    if (storyObject.epilogued) continue;
                    break;
                case "RESTORE":
                    if (!storyObject.HasLostPower()) continue;
                    break;
                case "RETIRE_CHECK":
                    if (storyObject.retired) continue;
                    break;
                case "RETIRED_ONLY":
                    if (!storyObject.retired) continue;
                    break;
            }
            holder.selectorActive = true;
            button.interactable = true;
            initialisedButtons.Add(button);
        }
        return initialisedButtons;
    }

    public void ClearCardButtons(List<Button> buttons){
        foreach (Button button in buttons){
            button.onClick.RemoveAllListeners();
            button.gameObject.SetActive(false);
        }
    }

    public void AddTextScrap(string text)
    {
        GameObject scrap = AddUI(textScrapPrefab);
        scrap.GetComponentInChildren<Text>().text = text;
    }

    public void AddDoubleTextScrap(string text1, string text2)
    {
        GameObject scrap = AddUI(doubleTextScrapPrefab);
        GameObject child;

        // Left and then right
        child = scrap.transform.GetChild(1).gameObject;
        if (text1 == "") child.SetActive(false);
        else child.GetComponentInChildren<Text>().text = text1;

        child = scrap.transform.GetChild(0).gameObject;
        if (text2 == "") child.SetActive(false);
        else child.GetComponentInChildren<Text>().text = text2;
    }

    public void AddTripleTextScrap(string[] texts)
    {
        GameObject scrap = AddUI(tripleTextScrapPrefab);
        GameObject child;

        // Left, centre and then right
        child = scrap.transform.GetChild(2).gameObject;
        if (texts[0] == "" || texts[0] == null) child.SetActive(false);
        else child.GetComponentInChildren<Text>().text = texts[0];

        child = scrap.transform.GetChild(1).gameObject;
        if (texts[1] == "" || texts[1] == null) child.SetActive(false);
        else child.GetComponentInChildren<Text>().text = texts[1];

        child = scrap.transform.GetChild(0).gameObject;
        if (texts[2] == "" || texts[2] == null) child.SetActive(false);
        else child.GetComponentInChildren<Text>().text = texts[2];
    }

    public GameObject AddUI(GameObject prefab){
        GameObject card = Instantiate(prefab);
        card.transform.SetParent(canvasContent, false);
        Vector3 position = card.transform.position;
        bookmarks.Add(canvasY);
        if (canvasY == 0) canvasY = card.transform.localScale.y / 2;
        else canvasY += card.transform.localScale.y;

        position.y = canvasY;
        card.transform.position = position;
        ScrollToBottom();
        return card;
    }

    public GameObject AddUIPrefab(string prefabName)
    {
        GameObject prefab = (GameObject)Resources.Load("PassagePrefabs/" + prefabName);
        if (prefab == null) Debug.LogWarning("Can't find prefab named " + prefabName);
        return AddUI(prefab);
    }

    public PassageEndHandler AddPassageEnd(){
        GameObject ender = AddUIPrefab("passage_end");
        return ender.GetComponent<PassageEndHandler>();
    }

    public void SetupStoryObjectButtons(){
        DestroyCardButtons();
        selectDelegate = null;
        List<StoryObject> objects = storyHolder.GetAll("All");
        createdButtons = new List<Button>();
        foreach(StoryObject storyObject in objects){
            Button button = AddButton(storyObject);
            button.onClick.AddListener(delegate { CreateStoryObjectCard(button, storyObject); });
            createdButtons.Add(button);
        }
    }

    public void CreateStoryObjectCard(Button button, StoryObject storyObject){
        if (objectCard != null) Destroy(objectCard);
        objectCard = Instantiate(storyObjectCardPrefab);
        bool chooseable = button.GetComponent<StoryObjectHolder>().selectorActive;
        objectCard.GetComponentInChildren<DisplayObjectHandler>().Init(storyObject, chooseable);
    }

    public Button AddButton(StoryObject storyObject){
        GameObject newObject = Instantiate(storyObjectChoicePrefab);
        newObject.transform.SetParent(rightContent, false);
        Vector3 position = newObject.transform.position;
        if (rightCanvasY == 0) rightCanvasY = newObject.transform.localScale.y / 2;
        else rightCanvasY += newObject.transform.localScale.y;

        position.y = rightCanvasY;
        newObject.transform.position = position;
        string display = storyObject.label;
        if (storyObject.retired) display = "Retired " + display;
        if (storyObject.named) display += "\n" + storyObject.GetName();

        Button button = newObject.GetComponentInChildren<Button>();
        button.GetComponentInChildren<Text>().text = display;
        button.GetComponentInChildren<StoryObjectHolder>().storyObject = storyObject;
        return button;
    }

    public void Select(StoryObject storyObject){
        selectedObject = storyObject;
        if (selectDelegate != null) selectDelegate();
        Destroy(objectCard);
    }

    [ContextMenu("Scroll to Bottom")]
    public void ScrollToBottom(){
        StartCoroutine(WaitForScrollChange());
    }

    IEnumerator WaitForScrollChange(){
        while (true){
            yield return null;
            if (scrollRect.verticalNormalizedPosition != 0f) break;
        }
        float speed = SCROLL_SPEED_BASE / canvasY;
        while (true){
            yield return new WaitForSeconds(SCROLL_WAIT_INTERVAL);
            scrollRect.verticalNormalizedPosition -= speed;

            if (scrollRect.verticalNormalizedPosition <= 0f){
                scrollRect.verticalNormalizedPosition = 0f;
                yield break;
            }
        }
    }

    public void ScrollToBookmark(){
        StartCoroutine(ScrollTo(bookmarks[currentBookmark]));
    }

    IEnumerator ScrollTo(double target){
        float speed = SCROLL_SPEED_BASE / canvasY;
        double destination = (canvasY - target) / canvasY;
        Debug.Log("Found destination " + destination.ToString());
        bool upwards = destination > scrollRect.verticalNormalizedPosition;

        while (true){
            yield return new WaitForSeconds(SCROLL_WAIT_INTERVAL);
            if (upwards)scrollRect.verticalNormalizedPosition -= speed;
            else scrollRect.verticalNormalizedPosition += speed;

            if ((upwards && scrollRect.verticalNormalizedPosition <= destination)
                || (!upwards && scrollRect.verticalNormalizedPosition >= destination)){
                scrollRect.verticalNormalizedPosition = (float) destination;
                yield break;
            }
        }
    }
}
