﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrewrittenHandler : Handler
{

    private Text typeDisplay;
    private Text textDisplay;
    private Button continueButton;

    public override void Awake(){
        base.Awake();
        int children = transform.childCount;
        GameObject child;

        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Type":
                    typeDisplay = child.GetComponent<Text>();
                    AddListeners(child);
                    break;
                case "Passage Display":
                    textDisplay = child.GetComponent<Text>();
                    break;
                case "Continue":
                    continueButton = child.GetComponent<Button>();
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }

    public override void Init(bool newCard, Passage passage){
        base.Init(newCard, passage);

        if (passage.tutorialOnly && !storyManager.gameManager.gameSettings.tutorials){
            Destroy(this.gameObject);
            storyManager.FinishScene();
            return;
        }

        passage.prefabName = "prewritten";
        typeDisplay.text = passage.label;
        textDisplay.text = passage.tooltip;
        if (newCard) {
            continueButton.onClick.AddListener(ReturnStory);
        } else {
            continueButton.gameObject.SetActive(false);
        }
    }

    public override void ReturnStory(){
        continueButton.gameObject.SetActive(false);
        if (!passage.tutorialOnly){
            passage.text = textDisplay.text;
            storyManager.storyHolder.story.Add(passage);
        }
        storyManager.FinishScene();
    }
}
