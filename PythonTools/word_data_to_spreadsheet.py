import os
import re

from collections import defaultdict, Counter
from pprint import pprint
from sys import argv
from copy import deepcopy

try:
    from simplejson import load
except ImportError:
    from json import load

PATTERN = re.compile(r"\[.*?\]")

BASE = r"C:\Users\gary.chadwick\Downloads\words.json"
BASE = r"C:\Users\Gary\Documents\Unity Projects\heros-journey\Assets\StreamingAssets\words.json"
BASE = r"C:\Users\gchadwick\Documents\05_Other\heros-journey\Assets\StreamingAssets\words.json"

OBJECT_ORDER = ("Setting", "Quest", "Hero", "Antagonist", "Ally", "Obstacle")

def extract_file(path):
    
    data = read_file(path)
    if not data: return

    folder = os.path.dirname(__file__)
    """
    phrases = data.pop("Phrases")
    path = os.path.join(folder, "Phrases.csv")
    with open(path, "w") as f:
        f.write("Type,Phrase\n")
        for key, strings in phrases.items():
            for string in strings:
                f.write(key + ',"' + string + '"\n')"""

    for key in data:
        if key == "Phrases":
            continue

        path = os.path.join(folder, key + ".csv")
        with open(path, 'w') as f:
            extra_columns = []
            tags = []
            for value in data[key].values():
                if isinstance(value, dict):
                    extra_columns += value.keys()
                    tags += value.get("tags", [])
            extra_columns = sorted(set(extra_columns))
            try:
                extra_columns.remove("tags")
            except:
                pass

            tags = sorted(set(tags))
            for word in OBJECT_ORDER:
                try:
                    tags.remove(word)
                except:
                    pass
            tags = list(OBJECT_ORDER) + tags
            columns = ["Word"] + extra_columns + tags
            f.write(",".join(columns))
            for word in sorted(data[key]):
                f.write("\n")
                values = data[key][word]
                f.write(word)
                for column in extra_columns:
                    f.write(",")
                    if column in values:
                        f.write(values[column] or "")
                for tag in tags:
                    f.write(",")
                    if tag in values.get("tags", []):
                        f.write("X")
        


def read_file(path):
    try:
        with open(path) as f:
            return load(f)
    except ValueError as e:
        print "Couldn't read JSON"
        print e


def main():
    if not argv[1:]:
       extract_file(BASE)
    else:
        for f in argv[1:]:
            print("Analysing " + f)
            extract_file(f)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print type(e), e, e.message
    raw_input("Complete")
