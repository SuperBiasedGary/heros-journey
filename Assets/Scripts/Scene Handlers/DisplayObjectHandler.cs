﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DisplayObjectHandler: MonoBehaviour{

    public StoryObject me = null;

    Text type;
    Text powerText;
    Text title;
    Text nameDisplay;
    Text descriptionDisplay;

    public Button close;
    public Button select;
    Button reroll;
    Button sampleButton;
    InputField nameInput;
    InputField descriptionInput;

    bool powered = false;
    bool rerollsEnabled;
    TooltipHolder currentTooltip;

    public void Awake(){
        
        int children = this.transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Type":
                    type = child.GetComponent<Text>();
                    break;
                case "Power":
                    powerText = child.GetComponent<Text>();
                    break;
                case "Name":
                    nameDisplay = child.GetComponent<Text>();
                    break;
                case "Description":
                    descriptionDisplay = child.GetComponent<Text>();
                    break;
                case "Select":
                    select = child.GetComponent<Button>();
                    break;
                case "Close":
                    close = child.GetComponent<Button>();
                    break;
            }
        }
    }

    public void Update(){
        if (Input.GetButton("Copy") &&
            (Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl))){
            GUIUtility.systemCopyBuffer = me.ToString();
        }
    }

    public void Init(StoryObject storyObject, bool chooseable){
        me = storyObject;

        powered = me.GetPower() > 0;
        if (!powered) powerText.gameObject.SetActive(false);
        else if (me.retired) powerText.text = "RETIRED";
        else powerText.text = "Power: " + me.GetPower();

        type.text = me.label;
        descriptionDisplay.text = me.details;

        if (me.named) nameDisplay.text = me.GetName();
        else nameDisplay.gameObject.SetActive(false);

        close.onClick.AddListener(DeleteCard);
        select.gameObject.SetActive(chooseable);
    }

    public void AddHandler(UnityAction action){
        select.gameObject.SetActive(true);
        select.onClick.AddListener(action);
    }

    void DeleteCard(){
        Destroy(this.transform.parent.gameObject);
    }

    public void SelectMe(){
        StoryManager storyManager = FindObjectOfType<StoryManager>();
        storyManager.Select(me);
    }
}
