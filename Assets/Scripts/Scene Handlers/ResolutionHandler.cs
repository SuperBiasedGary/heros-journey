﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResolutionHandler : Handler
{
    public GameObject startPrefab;
    public GameObject textScrapPrefab;
    public GameObject endPrefab;
    public GameObject coinFlipPrefab;
    private GameObject flipText;
    StoryObject storyObject;

    public Text preamble;

    public Button calculateButton;
    public Button flipButton;
    public Button submit;

    public float WAIT_TIME = 1f;
    public int coinPool = 0;
    public int roll = 0;
    Report report;

    public override void Awake(){
        base.Awake();
        int children = transform.childCount;
        GameObject child;
        for (int i = 0; i < children; i++){
            child = transform.GetChild(i).gameObject;
            switch (child.name){
                case "Preamble":
                    preamble = child.GetComponent<Text>();
                    AddListeners(child);
                    break;
                case "Calculate Pool":
                    calculateButton = child.GetComponent<Button>();
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }


    public override void Init(bool newCard, Passage passage, Recordable recordable){
        base.Init(newCard, passage);

        if (newCard){
            report = new Report();
            storyObject = storyManager.storyHolder.GetStoryObject(passage.objectType, passage.objectTypeFunction);
            preamble.text = "Now to determine if " + storyObject.GetName() + " can complete their goal...";
            report.header = preamble.text;
            calculateButton.onClick.AddListener(CalculatePool);
        } else {
            report = recordable as Report;
            preamble.text = report.header;
            foreach(Modifier modifier in report.modifiers){
                UpdatePool(modifier.modifier);
                storyManager.AddTextScrap(modifier.ToString());
            }
            FinishModifiers();
            storyManager.AddUI(coinFlipPrefab);
            Flip();
        }
    }

    void CalculatePool(){
        calculateButton.onClick.RemoveAllListeners();
        StartCoroutine(CalculateModifiers(WAIT_TIME));
    }

    IEnumerator CalculateModifiers(float waitTime){
        Modifier baseModifier = new Modifier();
        baseModifier.tooltip = "Starting pool equal to " + storyObject.GetName() + "'s Power.";
        storyManager.AddTextScrap(baseModifier.tooltip);
        baseModifier.modifier = storyObject.GetPower();
        report.modifiers.Add(baseModifier);
        UpdatePool(baseModifier.modifier);

        yield return new WaitForSeconds(waitTime);
        foreach (Modifier modifier in storyManager.storyHolder.modifiers){
            storyManager.AddTextScrap(modifier.ToString());
            report.modifiers.Add(modifier);
            UpdatePool(modifier.modifier);
            yield return new WaitForSeconds(waitTime);
        }
        FinishModifiers();

        GameObject buttonObject = storyManager.AddUI(coinFlipPrefab);
        flipButton = buttonObject.GetComponentInChildren<Button>();
        flipButton.onClick.AddListener(FlipCoins);
        AddListeners(flipButton.transform.GetChild(0).gameObject);
    }

    void FinishModifiers(){
        storyManager.AddTextScrap("Done calculating pool.");
        if (coinPool < 1){
            coinPool = 1;
            storyManager.AddTextScrap("Reset pool to 1.");
        }
        storyManager.AddTextScrap("Total Coin Pool: " + coinPool.ToString());
    }

    void UpdatePool(int change){
        coinPool += change;
    }

    void FlipCoins(){
        StartCoroutine(Flip(WAIT_TIME, true));
        flipButton.onClick.RemoveAllListeners();
    }

    void Flip(){
        storyManager.AddTextScrap("FLIPPING COINS");
        foreach(string flip in report.flips["Resolution"]){
            storyManager.AddTextScrap(flip);
        }
        CreateEnd(false);
    }


    IEnumerator Flip(float waitTime, bool updateReport){
        storyManager.AddTextScrap("FLIPPING COINS");

        string flip = "";
        if (updateReport) report.flips["Resolution"] = new string[coinPool];
        for (int i = 0; i < coinPool; i++){
            yield return new WaitForSeconds(waitTime);
            if (UnityEngine.Random.value > 0.5){
                roll++;
                flip = "HEADS";
            } else {
                flip = "TAILS";
            }

            storyManager.AddTextScrap(flip);
            if (updateReport) report.flips["Resolution"][i] = flip;
        }
        yield return new WaitForSeconds(waitTime * 2);

        if (updateReport){
            string result = Result();
            report.result = result;
        }
        CreateEnd(updateReport);
    }

    void CreateEnd(bool updateReport){
        GameObject child;
        GameObject resultObject = storyManager.AddUI(endPrefab);
        for (int i = 0; i < resultObject.transform.childCount; i++){
            child = resultObject.transform.GetChild(i).gameObject;
            switch (child.name){
                case "Result":
                    child.GetComponent<Text>().text = report.result;
                    break;
                case "Continue":
                    submit = child.GetComponent<Button>();
                    if (updateReport) submit.onClick.AddListener(ReturnStory);
                    AddListeners(child.transform.GetChild(0).gameObject);
                    break;
            }
        }
    }

    string Result(){
        string name = storyObject.GetName();
        storyManager.storyHolder.blockFlag = roll.ToString();
        switch (roll){
            case 0:
                return name + " has failed.";
            case 1:
                return name + " has barely succeeded.";
            case 2:
                return name + " has accomplished their goal, though it almost went poorly.";
            case 3:
                return name + " has accomplished their goal competently.";
            case 4:
                return name + " has soundly accomplished the goal beyond expectations.";
            default:
                storyManager.storyHolder.blockFlag = "5";
                return name + " has accomplished their goal and become legendary.";
        }
    }

    public override void ReturnStory(){
        report.prefabName = "resolution";
        storyManager.storyHolder.story.Add(report);
        storyManager.FinishScene();
    }
}
